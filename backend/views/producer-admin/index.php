<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User ;
use common\models\Producer ;
use common\models\Distribution ;

$this->setTitle('Producteurs') ;
$this->addBreadcrumb($this->getTitle()) ;
$this->addButton(['label' => 'Nouveau producteur <span class="glyphicon glyphicon-plus"></span>', 'url' => 'producer-admin/create', 'class' => 'btn btn-primary']) ;

?>

<div class="alert alert-info">
      Abonnements mensuels : <strong><?= $sumFreePrice ?> € HT</strong>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProviderProducer,
    'columns' => [
        'name',
        [
            'attribute' => 'date_creation',
            'format' => 'raw',
            'value' => function($model) {
                return date('d/m/Y', strtotime($model->date_creation)) ;
            }
        ],
        [
            'attribute' => 'Lieu',
            'format' => 'raw',
            'value' => function($model) {
                return Html::encode($model->city.' ('.$model->postcode.')') ;
            }
        ],
        [
            'attribute' => 'Utilisateurs',
            'format' => 'raw',
            'value' => function($model) {
                if(!$model->userProducer || !count($model->userProducer))
                {
                    return 'Aucun utilisateur' ;
                }
                else {
                    $users = count($model->userProducer).' client' ;
                    if(count($model->userProducer) > 1) {
                        $users .= 's' ;
                    }
                    return $users ;
                }
                    
            }
        ],
        [
            'attribute' => 'Contact',
            'format' => 'raw',
            'value' => function($model) {
                if(!isset($model->user) || (isset($model->user) && count($model->user) == 0)) 
                {
                    return 'Aucun contact' ;
                }
                else {
                    foreach($model->user as $u)
                    {
                        if($u->status == User::STATUS_PRODUCER)
                        {
                            return Html::encode($u->lastname.' '.$u->name)
                                .'<br />'.Html::encode($u->email)
                                .'<br />'.Html::encode($u->phone) ;
                        }
                    }
                    
                }
            }
        ],
        [
            'attribute' => 'active',
            'format' => 'raw',
            'value' => function($model) {
                $html = '' ;
                if($model->active) {
                    $html .= '<span class="label label-success">En ligne</span>' ;
                }
                else {
                    $html .= '<span class="label label-danger">Hors-ligne</span>' ;
                }
                    
                if(strlen($model->code))
                {
                    $html .= ' <span class="glyphicon glyphicon-lock" data-toggle="tooltip" data-placement="bottom" data-original-title="'.Html::encode($model->code).'"></span>' ;
                }
                
                return $html ;
            }
        ],
        [
            'attribute' => 'Prix libre',
            'label' => 'Prix libre',
            'format' => 'raw',
            'value' => function($model) {
                if(is_null($model->free_price)) {
                    return '' ;
                }
                else {
                    return $model->getFreePrice() ;
                }
                    
            }
        ],
        [
            'label' => 'Dons (mois précédent)',
            'format' => 'raw',
            'value' => function($model) {
                $productGift = Product::getProductGift() ;

                $res = Yii::$app->db->createCommand("SELECT SUM(product_order.price * product_order.quantity) AS total
                    FROM `order`, product_order, distribution
                    WHERE distribution.id_producer = :id_producer
                    AND `order`.id_distribution = distribution.id
                    AND `order`.id = product_order.id_order
                    AND distribution.date >= :date_start
                    AND distribution.date <= :date_end
                    AND product_order.id_product = :id_product_gift
                ")
                    ->bindValue(':id_producer', $model->id)
                    ->bindValue(':date_start', date('Y-m-01', strtotime("-1 month")))
                    ->bindValue(':date_end', date('Y-m-31', strtotime("-1 month")))
                    ->bindValue(':id_product_gift', $productGift->id)
                    ->queryOne();

                return Price::format($res['total']) ;

            }
        ],
    ],
]); ?>