<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\PointVenteUser ;

$this->setTitle('Points de vente') ;
$this->addBreadcrumb($this->getTitle()) ;
$this->addButton(['label' => 'Nouveau point de vente <span class="glyphicon glyphicon-plus"></span>', 'url' => 'point-sale/create', 'class' => 'btn btn-primary']) ;

?>

<div class="point-sale-index">
    <?= GridView::widget([
        'filterModel' => $searchModel, 
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            'locality',
            [
                'attribute' => 'delivery',
                'label' => 'Livraison',
                'filter' => [
                    'monday' => 'Lundi',
                    'tuesday' => 'Mardi',
                    'wednesday' => 'Mercredi',
                    'thursday' => 'Jeudi',
                    'friday' => 'Vendredi',
                    'saterday' => 'Samedi',
                    'sunday' => 'Dimanche',
                ],
                'value' => function($model) {
                    return $model->getStrDeliveryDays() ;
                }
            ],
            [
                'attribute' => 'access_type',
                'label' => 'Accès',
                'filter' => [
                    'open' => 'Ouvert',
                    'code' => 'Code',
                    'restricted_access' => 'Accès restreint'
                ],
                'format' => 'raw',
                'value' => function($model) {
                    $count = UserPointSale::find()->where(['id_point_sale' => $model->id])->count(); 
                    $html = '' ;
                    if($model->restricted_access)
                    {
                        $html .= '<span class="glyphicon glyphicon-lock"></span> ' ;
                        if($count == 1)
                        {
                            $html .= '1 utilisateur' ;
                        }
                        else {
                           $html .= $count.' utilisateurs' ;
                        }
                    }
                    
                    if(strlen($model->code))
                    {
                        if(strlen($html)) $html .= '<br />' ;
                        $html .= 'Code : <strong>'.Html::encode($model->code).'</strong>' ;
                    }
                    
                    return $html ;
                    
                }
            ],
            [
                'attribute' => 'credit',
                'label' => 'Crédit',
                'format' => 'raw',
                'value' => function($model) {
                    if($model->credit) {
                        return '<span class="glyphicon glyphicon-euro"></span>' ;
                    }
                        
                    return '' ;
                }
            ],
            [
                'attribute' => 'default',
                'label' => 'Par défaut',
                'format' => 'raw',
                'contentOptions' => ['class' => 'td-default'],
                'value' => function($model) {
                    if($model->default) {
                        return Html::a('<span class="glyphicon glyphicon-star"></span>', ['point-sale/default','id' => $model->id], [
                            'title' => Yii::t('app', 'Point de vente par défaut'), 'class' => 'btn btn-default'
                        ]);
                    }
                    else 
                    {
                        return Html::a('<span class="glyphicon glyphicon-star-empty"></span>', ['point-sale/default','id' => $model->id], [
                            'title' => Yii::t('app', 'Point de vente par défaut'), 'class' => 'btn btn-default'
                        ]);
                    }
                    
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'headerOptions' => ['class' => 'column-actions'],
                'contentOptions' => ['class' => 'column-actions'],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Modifier'), 'class' => 'btn btn-default'
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'Supprimer'), 'class' => 'btn btn-default'
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
