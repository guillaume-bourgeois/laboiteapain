<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Product;
use yii\helpers\ArrayHelper;
use common\models\TaxRate;
use common\models\Producer;
use common\helpers\GlobalParam;

?>

<div class="product-form">

        <?php $form = ActiveForm::begin([
                'enableClientValidation' => false,
                'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

        <div>
                <div class="col-md-8">
                        <?= $form->field($model, 'active')->radioList([1 => 'Oui', 0 => 'Non']) ?>
                        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
                        <?= $form->field($model, 'reference')->textInput(['maxlength' => 255]) ?>
                        <?= $form->field($model, 'id_product_category')->dropDownList(ProductCategory::populateDropdownList()); ?>
                        <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
                        <?= $form->field($model, 'recipe')->textarea()->label('Description longue') ?>

                        <?= $form->field($model, 'unit')
                                ->dropDownList(ArrayHelper::map(Product::$unitsArray, 'unit', 'wording'))
                                ->label('Unité (pièce, poids ou volume)'); ?>


                        <?php

                        //Récupère la tva par défaut du producteur courant
                        $producer = \common\helpers\GlobalParam::getCurrentProducer();
                        $taxRateDefault = $producer->taxRate;

                        $taxRateNamesArray = array_merge(array(0 => 'Tva par défaut'), ArrayHelper::map(TaxRate::find()->all(), 'id', function ($model) {
                                return $model->name;
                        }));
                        $taxRateValuesArray = array_merge(array(0 => $taxRateDefault->value), ArrayHelper::map(TaxRate::find()->all(), 'id', function ($model) {
                                return $model->value;
                        }));
                        foreach ($taxRateValuesArray as $key => $taxRateValue) {
                                $taxRateValuesArrayFormatted[$key] = array('data-tax-rate-value' => $taxRateValue);
                        }

                        ?>

                        <?php if($taxRateDefault->value != 0): ?>
                                <?= $form->field($model, 'id_tax_rate')->dropDownList($taxRateNamesArray, ['options' => $taxRateValuesArrayFormatted])->label('Taxe'); ?>
                        <?php endif; ?>

                        <?= $form->field($model, 'price', [
                                'template' => '
                        <div class="row">
                        <div class="col-xs-6">
                            <label for="product-price" class="control-label without-tax"></label>
                            <div class="input-group">
                            {input}<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <label for="product-price-with-tax" class="control-label with-tax"></label>
                            <div class="input-group">
                                <input type="text" id="product-price-with-tax" class="form-control" name="" value="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span>
                            </div>
                        </div>
                        </div>
                        
                    ',
                        ]) ?>
                        <?= $form->field($model, 'step')->textInput()->hint('Définit ce qui est ajouté ou enlevé lors des changements de quantité.') ?>

                        <?= $form->field($model, 'weight')->textInput()->label('Poids (g)') ?>

                        <div class="col-md-3">
                                <?= $form->field($model, 'quantity_max')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                                <?= $form->field($model, 'quantity_max_monday')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                                <?= $form->field($model, 'quantity_max_tuesday')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                                <?= $form->field($model, 'quantity_max_wednesday')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                                <?= $form->field($model, 'quantity_max_thursday')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                                <?= $form->field($model, 'quantity_max_friday')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                                <?= $form->field($model, 'quantity_max_saturday')->textInput() ?>
                        </div>
                        <div class="col-md-3">
                                <?= $form->field($model, 'quantity_max_sunday')->textInput() ?>
                        </div>
                        <div class="clr"></div>

                        <?php
                        if (!$model->isNewRecord) {
                                echo $form->field($model, 'apply_distributions')
                                        ->checkbox()
                                        ->hint('Sélectionnez cette option si vous souhaitez que ces modifications (actif / non actif, quantité max) soient répercutées dans les distributions à venir déjà initialisées.');
                        }
                        ?>
                </div>
                <div class="col-md-4">
                        <?= $form->field($model, 'photo')->fileInput() ?>
                        <?php
                        if (strlen($model->photo)) {
                                $url = Yii::$app->urlManagerProducer->getHostInfo() . '/' . Yii::$app->urlManagerProducer->baseUrl . '/uploads/' . $model->photo;
                                $url = str_replace('//uploads','/uploads', $url) ;
                                echo '<img class="photo-product" src="' . $url . '"  width="200px" /><br />';
                                echo '<input type="checkbox" name="delete_photo" id="delete_photo" /> <label for="delete_photo">Supprimer la photo</label><br /><br />';
                        }
                        ?>

                        <div id="days-production">
                                <h2>Jours de distribution</h2>
                                <?= $form->field($model, 'monday')->checkbox() ?>
                                <?= $form->field($model, 'tuesday')->checkbox() ?>
                                <?= $form->field($model, 'wednesday')->checkbox() ?>
                                <?= $form->field($model, 'thursday')->checkbox() ?>
                                <?= $form->field($model, 'friday')->checkbox() ?>
                                <?= $form->field($model, 'saturday')->checkbox() ?>
                                <?= $form->field($model, 'sunday')->checkbox() ?>
                        </div>
                        <div class="clr"></div>
                </div>
                <div class="clr"></div>
        </div>

        <?= $form->field($model, 'id_producer')->hiddenInput()->label('') ?>

        <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Ajouter' : 'Modifier', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

</div>
