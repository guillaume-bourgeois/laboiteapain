<?php

use common\helpers\Price ;

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

$this->setTitle('Liste des prix ('.Html::encode($model->name).')');
$this->addBreadcrumb(['label' => 'Produits', 'url' => ['index']]);
$this->addBreadcrumb(['label' => $model->name, 'url' => ['update', 'id' => $model->id]]);
$this->addBreadcrumb('Modifier');
//$this->addButton(['label' => 'Nouveau prix <span class="glyphicon glyphicon-plus"></span>', 'url' => ['product/prices-create', 'idProduct' => $model->id ], 'class' => 'btn btn-primary']);

?>

<?=
        $this->render('../_nav', [
                'model' => $model,
                'action' => $action,
        ]) ;
?>

<div class="col-md-12">

        <div class="panel panel-default">
                <div class="panel-heading">
                        <h3 class="panel-title">
                                Prix spécifiques à ce produit
                                <a href="<?= Yii::$app->urlManager->createUrl(['product/prices-create', 'idProduct' => $model->id ]) ?>" class="btn btn-default btn-xs">
                                        Nouveau prix
                                        <span class="glyphicon glyphicon-plus"></span>
                                </a>
                        </h3>
                </div>
                <div class="panel-body">
                        <?php

                        echo GridView::widget([
                                //'filterModel' => $searchModel,
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                        [
                                                'attribute' => 'id_user',
                                                'format' => 'raw',
                                                'value' => function ($model) {
                                                        if($model->user) {
                                                                return $model->user->getUsername() ;
                                                        }
                                                        return '<span class="label label-success">Tous</span>' ;
                                                }
                                        ],
                                        [
                                                'attribute' => 'id_user_group',
                                                'format' => 'raw',
                                                'value' => function ($model) {
                                                        if($model->userGroup) {
                                                                return $model->userGroup->name ;
                                                        }
                                                        return '<span class="label label-success">Tous</span>' ;
                                                }
                                        ],
                                        [
                                                'attribute' => 'id_point_sale',
                                                'format' => 'raw',
                                                'value' => function ($model) {
                                                        if($model->pointSale) {
                                                                return $model->pointSale->name ;
                                                        }
                                                        return '<span class="label label-success">Tous</span>' ;
                                                }
                                        ],
                                        [
                                                'attribute' => 'price',
                                                'value' => function ($productPrice) {
                                                        return Price::numberTwoDecimals($productPrice->price).' €' ;
                                                }
                                        ],
                                        [
                                                'attribute' => 'price',
                                                'header' => 'Prix (TTC)',
                                                'value' => function ($productPrice) use ($model) {
                                                        return Price::numberTwoDecimals(Price::getPriceWithTax($productPrice->price, $model->taxRate->value)).' €' ;
                                                }
                                        ],
                                        [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{update} {delete}',
                                                'headerOptions' => ['class' => 'column-actions'],
                                                'contentOptions' => ['class' => 'column-actions'],
                                                'buttons' => [
                                                        'update' => function ($url, $model) {
                                                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['product/prices-update', 'id' => $model->id], [
                                                                        'title' => Yii::t('app', 'Modifier'), 'class' => 'btn btn-default'
                                                                ]);
                                                        },
                                                        'delete' => function ($url, $model) {
                                                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['product/prices-delete', 'id' => $model->id], [
                                                                        'title' => Yii::t('app', 'Supprimer'), 'class' => 'btn btn-default'
                                                                ]);
                                                        }
                                                ],
                                        ],
                                ],
                        ]);

                        ?>
                </div>
        </div>

        <!--
        <div class="panel panel-default">
                <div class="panel-heading">
                        <h3 class="panel-title">Pourcentage global / Utilisateurs</h3>
                </div>
                <div class="panel-body">
                        <table class="table table-bordered">
                                <thead>
                                <tr>
                                        <th>Utilisateur</th>
                                        <th>Pourcentage</th>
                                        <th>Prix (HT)</th>
                                        <th>Prix (TTC)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($userProducerWithProductPercent && count($userProducerWithProductPercent) > 0): ?>
                                        <?php foreach($userProducerWithProductPercent as $userProducer): ?>
                                                <?php if($userProducer->user): ?>
                                                        <tr>
                                                                <td><?= $userProducer->user->getUsername() ?></td>
                                                                <td><?= $userProducer->product_price_percent ?> %</td>
                                                                <td><?= Price::format($model->getPrice(['user_producer' => $userProducer])) ?></td>
                                                                <td><?= Price::format($model->getPriceWithTax(['user_producer' => $userProducer])) ?></td>
                                                        </tr>
                                                <?php endif; ?>
                                        <?php endforeach; ?>
                                <?php else: ?>
                                        <tr><td colspan="4">Aucun résultat</td></tr>
                                <?php endif; ?>
                                </tbody>
                        </table>
                </div>
        </div>

        <div class="panel panel-default">
                <div class="panel-heading">
                        <h3 class="panel-title">Pourcentage global / Points de vente</h3>
                </div>
                <div class="panel-body">
                        <table class="table table-bordered">
                                <thead>
                                <tr>
                                        <th>Point de vente</th>
                                        <th>Pourcentage</th>
                                        <th>Prix (HT)</th>
                                        <th>Prix (TTC)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($pointSaleWithProductPercent && count($pointSaleWithProductPercent) > 0): ?>
                                        <?php foreach($pointSaleWithProductPercent as $pointSale): ?>
                                                <tr>
                                                        <td><?= Html::encode($pointSale->name) ?></td>
                                                        <td><?= $pointSale->product_price_percent ?> %</td>
                                                        <td><?= Price::format($model->getPrice(['point_sale' => $pointSale])) ?></td>
                                                        <td><?= Price::format($model->getPriceWithTax(['point_sale' => $pointSale])) ?></td>
                                                </tr>
                                        <?php endforeach; ?>
                                <?php else: ?>
                                        <tr><td colspan="4">Aucun résultat</td></tr>
                                <?php endif; ?>
                                </tbody>
                        </table>
                </div>
        </div>
        -->
        <div class="panel panel-default">
                <div class="panel-heading">
                        <h3 class="panel-title">Rappel du prix de base</h3>
                </div>
                <div class="panel-body">
                        <p>Prix de base : <strong><?= Price::format($model->getPrice()); ?> HT</strong> / <strong><?=  Price::format($model->getPriceWithTax()); ?> TTC</strong><br /></p>
                </div>
        </div>
</div>

<!--
<div class="col-md-4">

        <div class="panel panel-default">
                <div class="panel-heading">
                        <h3 class="panel-title">Priorités de résolution</h3>
                </div>
                <div class="panel-body">
                        <p>Le prix d'un produit se déduit dans un ordre précis de résolution, le voici : </p>
                        <ul>
                                <li>Les prix spécifiques définis au niveau du produit</li>
                                <li>Les pourcentages globaux définis au niveau des utilisateurs et points de vente</li>
                                <li>Le prix de base défini au niveau du produit</li>
                        </ul>
                        <p>À chaque étape de résolution, on vérifie si le contexte courant (utilisateur / point de vente) correspond à un prix.
                                Si c'est le cas, on l'utilise, sinon on passe à l'étape suivante jusqu'à arriver au prix de base
                                défini dans "Général".
                        </p>
                </div>
        </div>
</div>
-->