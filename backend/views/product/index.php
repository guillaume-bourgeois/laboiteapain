<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\Url;
use common\models\Product;
use common\models\TaxRate;
use common\models\Producer;
use common\helpers\GlobalParam;

$this->setTitle('Produits');
$this->addBreadcrumb($this->getTitle());
$this->addButton(['label' => 'Nouveau produit <span class="glyphicon glyphicon-plus"></span>', 'url' => 'product/create', 'class' => 'btn btn-primary']);


?>

<span style="display: none;" id="page-size"><?= $dataProvider->pagination->pageSize; ?></span>
<div class="product-index">
        <?= GridView::widget([
                'filterModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'columns' => [
                        [
                                'attribute' => 'order',
                                'headerOptions' => ['class' => 'order'],
                                'format' => 'raw',
                                'filter' => '',
                                'value' => function ($model) {
                                        return '<a class="btn-order btn btn-default" href="javascript:void(0);"><span class="glyphicon glyphicon-resize-vertical"></span></a>';
                                }
                        ],
                        [
                                'attribute' => 'photo',
                                'format' => 'raw',
                                'headerOptions' => ['class' => 'td-photo'],
                                'filter' => '',
                                'value' => function ($model) {
                                        if (strlen($model->photo)) {
                                                $url = Yii::$app->urlManagerProducer->getHostInfo() . '/' . Yii::$app->urlManagerProducer->baseUrl . '/uploads/'.$model->photo ;
                                                $url = str_replace('//uploads','/uploads', $url) ;
                                                return '<img class="photo-product" src="' . $url . '" />';
                                        }
                                        return '';
                                }
                        ],
                        'name',
                        'description',
                        [
                                'attribute' => 'id_product_category',
                                'format' => 'raw',
                                'headerOptions' => ['class' => 'td-product-category'],
                                'filter' => '',
                                'value' => function ($model) {
                                        if ($model->productCategory) {
                                            return $model->productCategory->name ;
                                        }
                                        return '';
                                }
                        ],
                        [
                                'attribute' => 'id_tax_rate',
                                'value' => function ($model) {
                                        if ($model->id_tax_rate == 0 || $model->id_tax_rate == null) {

                                                //Récupère la tva par défaut du producteur courant
                                                $taxRateDefault = GlobalParam::getCurrentProducer()->taxRate;

                                                $return = $taxRateDefault->name;
                                        } else {

                                                $return = $model->taxRate->name;
                                        }
                                        return $return;
                                }
                        ],
                        [
                                'attribute' => 'price',
                                'value' => function ($model) {
                                        $return = '';
                                        if ($model->price) {
                                                $return = Price::format($model->getPriceWithTax()) . ' (' . Product::strUnit($model->unit, 'wording_unit', true) . ')';
                                        }

                                        return $return;
                                }
                        ],
                        [
                                'attribute' => 'active',
                                'headerOptions' => ['class' => 'active'],
                                'contentOptions' => ['class' => 'center'],
                                'format' => 'raw',
                                'filter' => [0 => 'Non', 1 => 'Oui'],
                                'value' => function ($model) {
                                        if ($model->active) {
                                                return '<span class="label label-success">oui</span>';
                                        } else {
                                                return '<span class="label label-danger">non</span>';
                                        }
                                }
                        ],

                        [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}',
                                'headerOptions' => ['class' => 'column-actions'],
                                'contentOptions' => ['class' => 'column-actions'],
                                'buttons' => [
                                        'update' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                                        'title' => Yii::t('app', 'Modifier'), 'class' => 'btn btn-default'
                                                ]);
                                        },
                                        'delete' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                                        'title' => Yii::t('app', 'Supprimer'), 'class' => 'btn btn-default'
                                                ]);
                                        }
                                ],
                        ],
                ],
        ]); ?>
</div>
