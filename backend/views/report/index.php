<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

\backend\assets\VuejsReportIndexAsset::register($this);

$this->setTitle('Rapports') ;
$this->addBreadcrumb('Rapports') ;

?>
<div id="app-report-index" class="app-vuejs">
    <div id="loading" v-if="showLoading">
        <img src="<?= Yii::$app->urlManagerBackend->getBaseUrl(); ?>/img/loader.gif" alt="Chargement ..." />
    </div>
    <div id="wrapper-app-report-index" :class="'wrapper-app-vuejs '+(loading ? '' : 'loaded')">
        <div id="parameters" class="col-md-6">
            <div id="nav-sections-report">
                <a v-for="section in sections" :class="'btn-section btn '+(currentSection == section.id ? 'btn-primary' : 'btn-default')" @click="changeSection(section)"><span :class="'fa '+section.icon"></span> {{ section.name }}<span v-if="currentSection == section.id"> <span class="glyphicon glyphicon-triangle-bottom"></span></span></a>
            </div>
            <div id="section-users" class="panel panel-default section" v-show="currentSection == 'users'">
                <div class="panel-body">
                    <div id="wrapper-search-user">
                        <span class="glyphicon glyphicon-search"></span>
                        <input type="text" class="form-control" id="input-search-user" placeholder="Rechercher" v-model="termSearchUser" />
                    </div>
                    <div class="content-max-height">
                        <ul id="list-users">
                            <li v-for="user in usersArray" v-if="!termSearchUser.length || (termSearchUser.length && (user.lastname.toLowerCase().indexOf(termSearchUser.toLowerCase()) != -1 || user.name.toLowerCase().indexOf(termSearchUser.toLowerCase()) != -1 ))">
                                <input type="checkbox" :id="'user_'+user.user_id" v-model="user.checked" @change="reportChange()" />
                                <label :for="'user_'+user.user_id" v-html="(user.name_legal_person && user.name_legal_person.length) ? user.name_legal_person : user.lastname+' '+user.name"></label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="section-points-sale" class="panel panel-default section" v-show="currentSection == 'points-sale'">
                <div class="panel-body">
                    <div class="content-max-height">
                        <ul class="list" id="list-points-sale">
                            <li v-for="pointSale in pointsSaleArray">
                                <input type="checkbox" :id="'pointsale_'+pointSale.id" v-model="pointSale.checked" @change="reportChange()" /> 
                                <label :for="'pointsale_'+pointSale.id" v-html="pointSale.name"></label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="section-distributions" class="panel panel-default section" v-show="currentSection == 'distributions'">
                <div class="panel-body">
                    <div id="wrapper-select-year">
                        <select class="form-control" v-model="distributionYear">                        
                            <option v-for="year in distributionYearsArray">{{ year }}</option>
                        </select>
                    </div>
                    <div class="content-max-height">
                        <div class="distribution-month" v-for="(distributionsMonth, month) in distributionsByMonthArray" v-if="distributionsMonth.year == distributionYear && distributionsMonth.distributions.length > 0">
                            <a class="btn btn-default link-month-distribution" @click="distributionsMonth.display = !distributionsMonth.display">{{ distributionsMonth.month }} <span class="label label-success" v-if="countDistributionsByMonth(month)">{{ countDistributionsByMonth(month) }}</span> <span class="glyphicon glyphicon-menu-down"></span></a>
                            <div class="the-distributions" v-if="distributionsMonth.display">
                                <a @click="selectDistributions(month)" class="btn btn-default btn-xs btn-select"><span class="glyphicon glyphicon-check"></span> Tout <span v-if="countDistributionsByMonth(month)">déselectionner</span><span v-else>sélectionner</span></a> 
                                <ul>
                                    <li v-for="distribution in distributionsMonth.distributions">
                                        <input type="checkbox" :id="'distribution_'+distribution.id" v-model="distribution.checked" @change="reportChange()" />
                                        <label :for="'distribution_'+distribution.id">{{ distribution.date }}</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div id="report" class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">Rapport</h2>
                </div>
                <div class="panel-body">
                    <div id="summary">
                        <div class="alert alert-info" v-if="!countUsers() && !countPointsSale() && !countDistributions()">
                            Veuillez sélectionner un utilisateur et/ou un point de vente et/ou une distribution.
                        </div>
                        <div class="section" v-if="countUsers() > 0">
                            <h3><span class="fa fa-users"></span> {{ countUsers() }} Utilisateurs</h3>
                            <template v-for="user in usersArray" v-if="user.checked">
                                <span v-html="user.name+' '+user.lastname"></span><span class="comma">, </span>
                            </template>
                        </div>
                        <div class="section" v-if="countPointsSale() > 0">
                            <h3><span class="fa fa-map-marker"></span> {{ countPointsSale() }} Points de vente</h3>
                            <template v-for="pointSale in pointsSaleArray" v-if="pointSale.checked">
                                <span v-html="pointSale.name"></span><span class="comma">, </span>
                            </template>
                        </div>
                        <div class="section" v-if="countDistributions() > 0">
                            <h3><span class="fa fa-calendar"></span> {{ countDistributions() }} Distributions</h3>
                            <template v-for="distributionsMonth in distributionsByMonthArray">
                                <template v-for="distribution in distributionsMonth.distributions" v-if="distribution.checked">
                                    <span v-html="distribution.date"></span><span class="comma">, </span>
                                </template>
                            </template>
                        </div>
                    </div>
                    <div id="report" v-if="countUsers() || countPointsSale() || countDistributions()">
                        <div class="content-report section" v-if="showReport">
                            <h3><span class="fa fa-pencil-square-o"></span> Rapport</h3>
                            <table class="table table-bordered" v-if="tableReport.length > 1">
                                <thead>
                                    <tr>
                                        <th>Produit</th>
                                        <th>Quantité</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="line in tableReport">
                                        <td>{{ line.name }}</td>
                                        <td>{{ line.quantity }}</td>
                                        <td v-html="line.total"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="alert alert-warning" v-else>
                                Aucune donnée disponible pour ces critères.
                            </div>
                        </div>
                        <a class="btn btn-primary" @click="generateReport()" v-else="!showReport"><span class="fa fa-pencil-square-o"></span> Générer</a>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>