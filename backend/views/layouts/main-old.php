<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\models\Producer;
use common\models\User;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\helpers\Url;
use common\helpers\GlobalParam;

/* @var $this \yii\web\View */
/* @var $content string */

\common\assets\CommonAsset::register($this);
\backend\assets\AppAsset::register($this);

$producer = null;
if (!Yii::$app->user->isGuest) {
        $producer = Producer::findOne(GlobalParam::getCurrentProducerId());
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="baseurl" content="<?= Yii::$app->urlManagerBackend->baseUrl; ?>">
    <meta name="baseurl-absolute"
          content="<?= Yii::$app->urlManagerBackend->getHostInfo() . Yii::$app->urlManagerBackend->baseUrl; ?>">
    <link rel="icon" type="image/png" href="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/favicon3.png"/>
        <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> - distrib</title>
        <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
        <?php
        NavBar::begin([
                'brandLabel' => '<img class="logo" src="' . Yii::$app->urlManager->getBaseUrl() . '/img/laboulange3.png" />',
                'brandUrl' => Yii::$app->homeUrl,
                'innerContainerOptions' => ['class' => 'container-fluid'],
                'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top nav-header',
                ],
        ]);

        $menuItems = [
                [
                        'label' => '<span class="glyphicon glyphicon-home"></span> Tableau de bord',
                        'url' => ['/site/index'],
                        'visible' => !Yii::$app->user->isGuest
                ],
                [
                        'label' => '<span class="glyphicon glyphicon-calendar"></span> Commandes',
                        'url' => ['/order/index'],
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                                [
                                        'label' => '<span class="glyphicon glyphicon-calendar"></span> Toutes les commandes',
                                        'url' => ['/order/index'],
                                        'visible' => !Yii::$app->user->isGuest
                                ],
                                [
                                        'label' => '<span class="glyphicon glyphicon-repeat"></span> Abonnements',
                                        'url' => ['/subscription/index'],
                                        'visible' => !Yii::$app->user->isGuest
                                ],
                        ]
                ],
                [
                        'label' => '<span class="glyphicon glyphicon-grain"></span> Produits',
                        'url' => ['/product/index'],
                        'visible' => !Yii::$app->user->isGuest
                ],
                [
                        'label' => '<span class="glyphicon glyphicon-map-marker"></span> Points de vente',
                        'url' => ['/point-sale/index'],
                        'visible' => !Yii::$app->user->isGuest
                ],
                [
                        'label' => '<span class="glyphicon glyphicon-user"></span> Clients',
                        'url' => ['/user/index'],
                        'visible' => !Yii::$app->user->isGuest
                ],
                [
                        'label' => '<span class="glyphicon glyphicon-plus"></span>',
                        'url' => ['/producer/update'],
                        'visible' => !Yii::$app->user->isGuest,
                        'items' => [
                                [
                                        'label' => '<span class="glyphicon glyphicon-cog"></span> Paramètres',
                                        'url' => ['/producer/update'],
                                        'visible' => !Yii::$app->user->isGuest
                                ],
                                [
                                        'label' => '<span class="glyphicon glyphicon-bullhorn"></span> Communiquer',
                                        'url' => ['/communicate/index'],
                                        'visible' => !Yii::$app->user->isGuest
                                ],
                                [
                                        'label' => '<span class="glyphicon glyphicon-euro"></span> Mon abonnement',
                                        'url' => ['/producer/billing'],
                                        'visible' => !Yii::$app->user->isGuest,
                                ],
                                [
                                        'label' => '<span class="glyphicon glyphicon-stats"></span> Statistiques',
                                        'url' => ['/stats/index'],
                                        'visible' => !Yii::$app->user->isGuest,
                                ],
                                [
                                        'label' => '<span class="glyphicon glyphicon-stats"></span> Statistiques produits',
                                        'url' => ['/stats/products'],
                                        'visible' => !Yii::$app->user->isGuest,
                                ],
                                [
                                        'label' => '<span class="glyphicon glyphicon-wrench"></span> Développement',
                                        'url' => ['/development/index'],
                                        'visible' => !Yii::$app->user->isGuest
                                ],
                        ],
                ]
        ];

        if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Connexion', 'url' => ['/site/login']];
        } else {

                if (Yii::$app->user->identity->status == USER::STATUS_ADMIN) {
                        $menuItems[] = [
                                'label' => '<span class="glyphicon glyphicon-asterisk"></span>',
                                'url' => '#',
                                'items' => [
                                        [
                                                'label' => '<span class="glyphicon glyphicon-th-list"></span> Producteurs',
                                                'url' => ['producer-admin/index'],
                                                'visible' => !Yii::$app->user->isGuest,
                                        ],
                                        [
                                                'label' => '<span class="glyphicon glyphicon-euro"></span> Facturation',
                                                'url' => ['producer-admin/billing'],
                                                'visible' => false,
                                        ],
                                ]
                        ];
                }

                $menuItems[] = [
                        'label' => '<span class="glyphicon glyphicon-off"></span>',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post', 'title' => 'Déconnexion']
                ];

                $menuItems[] = [
                        'label' => '<span class="retour-site">Retour sur le site</span>',
                        'url' => Yii::$app->urlManagerProducer->createAbsoluteUrl(['site/index', 'slug_producer' => $producer->slug]),
                ];
        }
        echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
                'encodeLabels' => false
        ]);
        NavBar::end();
        ?>

    <div class="container-fluid container-body">
            <?php if (YII_ENV == 'dev' || YII_ENV == 'demo'): ?>
                <div id="env-dev"><?php if (YII_ENV == 'dev'): ?>Dév.<?php elseif (YII_ENV == 'demo'): ?>Démo<?php endif; ?></div>
            <?php endif; ?>
            <?php if (!Yii::$app->user->isGuest): ?>
                <div class="name-producer">
                        <?php if (User::getCurrentStatus() == User::STATUS_PRODUCER): ?>
                            <span><?= Html::encode(Yii::$app->user->identity->getNameProducer()); ?></span>
                        <?php elseif (User::getCurrentStatus() == User::STATUS_ADMIN): ?>
                                <?php $form = ActiveForm::begin(['id' => 'select-producer']); ?>
                                <?=
                                Html::dropDownList('select_producer', GlobalParam::getCurrentProducerId(), ArrayHelper::map(Producer::find()->orderBy('name ASC')->all(), 'id', function ($model, $defaultValue) {
                                        return $model->name;
                                }));
                                ?>
                                <?php ActiveForm::end(); ?>
                        <?php endif; ?>

                        <?php
                        $producer = Producer::findOne(GlobalParam::getCurrentProducerId());
                        if (!$producer->active):
                                ?>
                            <span class="label label-danger" data-toggle="tooltip" data-placement="bottom"
                                  data-original-title="Activez votre établissement quand vous le souhaitez afin de la rendre visible à vos clients.">
                            <?= Html::a('Hors-ligne', ['producer/update']); ?>
                            </span>
                        <?php endif; ?>

                    <div class="clr"></div>
                </div>
            <?php endif; ?>

            <?php if (YII_ENV == 'demo'): ?>
                <div id="block-demo">
                    <div class="container-fluid">
                        <span class="glyphicon glyphicon-eye-open"></span> <strong>Espace de démonstration</strong> :
                        Testez la plateforme sans avoir à vous inscrire. Les données sont réinitialisées quotidiennement
                        &bull; <?= Html::a('Retour', Url::env('prod', 'frontend')) ?>
                    </div>
                </div>
            <?php endif; ?>

            <?=
            Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
            <?= $content ?>
    </div>
</div>

<div id="alerts-fixed"></div>

<footer class="footer">
    <div class="container-fluid">
        <p class="pull-left">
            <a href="<?php echo Url::frontend('site/contact'); ?>">Contact</a> &bull;
            <a href="<?php echo Url::frontend('site/mentions'); ?>">Mentions légales</a> &bull;
            <a href="<?php echo Url::frontend('site/cgv'); ?>">CGS</a>
            <a id="code-source" href="https://framagit.org/guillaume-bourgeois/laboiteapain">Code source <img
                        src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-framagit.png"
                        alt="Hébergé par Framasoft"/> <img
                        src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-gitlab.png"
                        alt="Propulsé par Gitlab"/></a>
        </p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>

<!-- analytics -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-86917043-1', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>
<?php $this->endPage() ?>
