<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Product ;
use common\helpers\GlobalParam ;


$this->setTitle('Abonnements') ;
$this->addBreadcrumb($this->getTitle()) ;
$this->addButton(['label' => 'Nouvel abonnement <span class="glyphicon glyphicon-plus"></span>', 'url' => 'subscription/create', 'class' => 'btn btn-primary']) ;

$subscriptionsArray = Subscription::searchAll() ;



?>
<div class="subscription-index">
    
    <?= GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'username',
                'label' => 'Utilisateur',
                'format' => 'raw',
                'value' => function($model) {
                    if(strlen($model->username))
                    {
                        return Html::encode($model->username) ;
                    }
                    else {
                        if(isset($model->user)) {
                            return Html::encode($model->user->lastname.' '.$model->user->name) ;
                        }
                    }
                }
            ],
            [
                'attribute' => 'product_name',
                'label' => 'Produits',
                'format' => 'raw',
                'value' => function($model) {
                    $html = '' ;
                    foreach($model->productSubscription as $productSubscription)
                    {
                        if(isset($productSubscription->product)) {
                            $html .= Html::encode($productSubscription->product->name).' ('.($productSubscription->quantity * Product::$unitsArray[$productSubscription->product->unit]['coefficient']).'&nbsp;'.Product::strUnit($productSubscription->product->unit, 'wording_short').')<br />' ;
                        }
                        else {
                            $html .= 'Produit non défini<br />'  ;
                        }
                    }
                    
                    // aucun produit
                    if(!count($model->productSubscription))
                    {
                        $html .= '<span class="glyphicon glyphicon-warning-sign"></span> Aucun produit<br />' ;
                    }

                    return $html ;
                }
            ],
            [
                'attribute' => 'id_point_sale',
                'label' => 'Point de vente',
                'format' => 'raw',
                'filter' => ArrayHelper::map(PointSale::find()->where(['id_producer' => GlobalParam::getCurrentProducerId()])->asArray()->all(), 'id', 'name'),
                'value' => function($model) {
                    return Html::encode($model->pointSale->name) ;
                }
            ],
            [
                'attribute' => 'date_begin',
                'label' => 'Période',
                'format' => 'raw',
                'value' => function($model) {
                    $html = '<small>' ;
                    if($model->date_end) {
                        $html .= 'Du&nbsp;' ;
                    }
                    else {
                        $html .= 'À partir du ' ;
                    }
                    $html .= '</small>' ;
                    $html .= date('d/m/Y',strtotime($model->date_begin)) ;
                    if($model->date_end) {
                        $html .= '<br />au&nbsp;'.date('d/m/Y',strtotime($model->date_end)) ;
                        if(date('Y-m-d') > $model->date_end) {
                            $html .= ' <span class="label label-danger">Terminé</span>' ;
                        }
                    }

                    return $html ;
                }
            ],
            [
                'attribute' => 'day',
                'label' => 'Jours',
                'format' => 'raw',
                'filter' => [
                    'monday' => 'Lundi',
                    'tuesday' => 'Mardi',
                    'wednesday' => 'Mercredi',
                    'thursday' => 'Jeudi',
                    'friday' => 'Vendredi',
                    'saterday' => 'Samedi',
                    'sunday' => 'Dimanche',
                ],
                'contentOptions' => ['class' => 'text-small'],
                'value' => function($model) {
                    $html = '' ;
                    if($model->monday) {
                        $html .= 'lundi, ' ;
                    }
                    if($model->tuesday) {
                        $html .= 'mardi, ' ;
                    }
                    if($model->wednesday) {
                        $html .= 'mercredi, ' ;
                    }
                    if($model->thursday) {
                        $html .= 'jeudi, ' ;
                    }
                    if($model->friday) {
                        $html .= 'vendredi, ' ;
                    }
                    if($model->saturday) {
                        $html .= 'samedi, ' ;
                    }
                    if($model->sunday) {
                        $html .= 'dimanche, ' ;
                    }
                    
                    if(strlen($html)) {
                        return substr ($html, 0, strlen($html) - 2) ;
                    }
                    else {
                        return '<span class="glyphicon glyphicon-warning-sign"></span> Aucun jour' ;
                    }
                }
            ],
            [
                'attribute' => 'week_frequency',
                'filter' => [
                    1 => 'Toutes les semaines', 
                    2 => 'Toutes les 2 semaines', 
                    3 => 'Toutes les 3 semaines',
                    4 => 'Tous les mois'],
                'value' => function($model) {
                    if($model->week_frequency == 1) {
                        return 'Toutes les semaines' ;
                    }
                    else {
                        return 'Toutes les '.$model->week_frequency.' semaines' ;
                    }
                        
                }
            ],
            [
                'attribute' => 'auto_payment',
                'format' => 'raw',
                'label' => 'Paiement automatique',
                'headerOptions' => ['class' => 'column-auto-payment'],
                'contentOptions' => ['class' => 'column-auto-payment'],
                'filter' => [0 => 'Non', 1 => 'Oui'],
                'value' => function($model) {
                    if($model->auto_payment) {
                        return '<span class="label label-success">Oui</span>' ;
                    }
                    else {
                        return '<span class="label label-danger">Non</span>' ;
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'headerOptions' => ['class' => 'column-actions'],
                'contentOptions' => ['class' => 'column-actions'],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Modifier'), 'class' => 'btn btn-default'
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'Supprimer'), 'class' => 'btn btn-default'
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
