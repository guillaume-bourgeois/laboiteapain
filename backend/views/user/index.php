<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use common\models\Order;

$this->setTitle('Utilisateurs');
$this->addBreadcrumb($this->getTitle());
$this->addButton(['label' => 'Nouvel utilisateur <span class="glyphicon glyphicon-plus"></span>', 'url' => 'user/create', 'class' => 'btn btn-primary']);

?>

<?=

$this->render('_menu', [
        'idPointSaleActive' => $idPointSaleActive,
        'sectionInactiveUsers' => $sectionInactiveUsers,
        'sectionSubscribers' => $sectionSubscribers,
        'pointsSaleArray' => $pointsSaleArray,
        'section' => 'index'
]);

?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                [
                        'attribute' => 'username',
                        'label' => 'Nom',
                        'value' => function ($model) {
                                if (isset($model['name_legal_person']) && strlen($model['name_legal_person'])) {
                                        return $model['name_legal_person'];
                                } else {
                                        return $model['lastname'] . ' ' . $model['name'];
                                }
                        }
                ],
                [
                        'attribute' => 'contacts',
                        'header' => 'Contacts',
                        'format' => 'raw',
                        'value' => function ($model) {
                                $html = '';
                                if (strlen($model['phone'])) {
                                        $html .= $model['phone'];
                                }
                                if (strlen($model['phone']) && strlen($model['email'])) {
                                        $html .= '<br />';
                                }
                                if (strlen($model['email'])) {
                                        $html .= $model['email'];
                                }
                                return $html;
                        }
                ],
                [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Commandes',
                        'template' => '{orders}',
                        'headerOptions' => ['class' => 'actions'],
                        'buttons' => [
                                'orders' => function ($url, $model) {
                                        $url = Yii::$app->urlManager->createUrl(['user/orders', 'id' => $model['id']]);
                                        $countOrders = Order::searchCount([
                                                'id_user' => $model['id']
                                        ], ['conditions' => 'date_delete IS NULL']);

                                        $html = '';
                                        if ($countOrders) {
                                                $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span> ' . $countOrders, $url, [
                                                        'title' => Yii::t('app', 'Commandes'), 'class' => 'btn btn-default '
                                                ]);;
                                        } else {
                                                $html .= 'Aucune commande';
                                        }

                                        return $html;
                                },
                        ],
                ],
                [
                        'attribute' => 'credit',
                        'format' => 'raw',
                        'value' => function ($model) use ($producer) {

                                $userProducer = UserProducer::searchOne([
                                        'id_user' => $model->id
                                ]);
                                $credit = $userProducer ? $userProducer->credit : 0;
                                $classBtnCredit = $userProducer->credit_active ? 'btn-success' : 'btn-default';

                                $html = '<div class="input-group">
                        <input type="text" class="form-control input-credit" readonly="readonly" value="' . number_format($credit, 2) . ' €" placeholder="">
                        <span class="input-group-btn">
                          ' . Html::a(
                                                '<span class="glyphicon glyphicon-euro"></span>',
                                                Yii::$app->urlManager->createUrl(['user/credit', 'id' => $model->id]),
                                                [
                                                        'title' => 'Crédit',
                                                        'class' => 'btn ' . $classBtnCredit
                                                ]
                                        ) . '
                        </span>
                      </div>';
                                return $html;
                        }
                ],
                [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'headerOptions' => ['class' => 'column-actions'],
                        'contentOptions' => ['class' => 'column-actions'],
                        'buttons' => [
                                'update' => function ($url, $model) {
                                        $url = Yii::$app->urlManager->createUrl(['user/update', 'id' => $model->id]);
                                        $user = User::find()->with('userProducer')->where(['id' => $model->id])->one();
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                                'title' => Yii::t('app', 'Modifier'), 'class' => 'btn btn-default'
                                        ]);
                                },
                                'delete' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Yii::$app->urlManager->createUrl(array_merge(['user/delete', 'id' => $model->id], Yii::$app->getRequest()->getQueryParams())), [
                                                'title' => Yii::t('app', 'Supprimer'), 'class' => 'btn btn-default btn-confirm-delete'
                                        ]);
                                }
                        ],
                ],
        ],
]); ?>
