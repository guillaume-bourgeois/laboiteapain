<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\CreditHistory; 
use common\models\Producer;
use common\helpers\GlobalParam;

$this->setTitle('Créditer <small>'.Html::encode($user->lastname.' '.$user->name).'</small>', 'Créditer '.Html::encode($user->lastname.' '.$user->name)) ;
$this->addBreadcrumb(['label' => 'Utilisateurs', 'url' => ['index']]) ;
$this->addBreadcrumb(['label' => Html::encode($user->lastname.' '.$user->name)]) ;
$this->addBreadcrumb('Créditer') ;

?>

<div class="user-credit">
    
    <?php
        $producer = Producer::searchOne([
            'id' => GlobalParam::getCurrentProducerId()
        ]);
        
        if(!$producer->credit)
        {
            echo '<div class="alert alert-warning">Attention, la fonctionnalité <strong>Crédit</strong> est désactivée dans vos <a href="'.Yii::$app->urlManager->createurl(['producer/update']).'">paramètres</a>.'
                    . ' Pensez à l\'activer si vous souhaitez qu\'elle soit visible de vos utilisateurs.</div>' ;
        }
    ?>
    
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Crédit obligatoire
                    <?= Html::a($userProducer->credit_active ? 'Désactiver' : 'Activer', ['user/state-credit', 'idUser' => $user->id,'state' => !$userProducer->credit_active], ['class' => 'btn btn-default btn-xs']); ?>
                </h3>
            </div>
            <div class="panel-body">
                <?php if($userProducer->credit_active): ?>
                    <div class="alert alert-success">Activé</div>
                <?php else: ?>
                    <div class="alert alert-danger">Désactivé</div>
                <?php endif; ?>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Crédit / débit</h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($creditForm, 'type')->dropDownList([
                    CreditHistory::TYPE_CREDIT => 'Crédit',
                    CreditHistory::TYPE_DEBIT => 'Débit',
                ]) ?>
                <?= $form->field($creditForm, 'amount')->textInput() ?>
                <?= $form->field($creditForm, 'mean_payment')->dropDownList([
                    MeanPayment::MONEY => MeanPayment::getStrBy(MeanPayment::MONEY),
                    MeanPayment::CREDIT_CARD => MeanPayment::getStrBy(MeanPayment::CREDIT_CARD),
                    MeanPayment::CHEQUE => MeanPayment::getStrBy(MeanPayment::CHEQUE),
                    MeanPayment::TRANSFER => MeanPayment::getStrBy(MeanPayment::TRANSFER),
                    MeanPayment::OTHER => MeanPayment::getStrBy(MeanPayment::OTHER),
                ]) ?>
                <?= $form->field($creditForm, 'comment')->textarea() ?>
                <?= $form->field($creditForm, 'send_mail')->checkbox() ?>

                <div class="form-group">
                    <?= Html::submitButton( 'Créditer', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    
    <div class="col-md-8">
        <h2>Historique <span class="the-credit"><?= number_format($user->getCredit($producer->id), 2); ?> €</span></h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Utilisateur</th>
                    <th>Type</th>
                    <th>- Débit</th>
                    <th>+ Crédit</th>
                    <th>Paiement</th>
                    <th>Commentaire</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($history)): ?>
                    <?php foreach($history as $creditHistory): ?>
                        <tr>
                            <td><?= $creditHistory->getDate(true) ; ?></td>
                            <td><?= Html::encode($creditHistory->strUserAction()); ?></td>
                            <td><?= $creditHistory->getStrWording(); ?></td>
                            <td>
                                <?php if($creditHistory->isTypeDebit()): ?>
                                    - <?= $creditHistory->getAmount(true); ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if($creditHistory->isTypeCredit()): ?>
                                    + <?= $creditHistory->getAmount(true); ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?= $creditHistory->getStrMeanPayment() ?>
                            </td>
                            <td>
                                <?php if(strlen($creditHistory->comment)): ?>
                                    <?= nl2br($creditHistory->comment) ; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><td colspan="4">Aucun résultat</td></tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
    
</div>
