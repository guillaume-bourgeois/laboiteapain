<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Product;
use yii\helpers\ArrayHelper;
use common\models\TaxRate;
use common\models\Producer;

\backend\assets\VuejsDocumentFormAsset::register($this);

?>

<div class="document-form" id="app-document-form" data-class-document="<?= $model->getClass() ?>"
     data-id-document="<?= ($model->id > 0) ? $model->id : $model->id ?>">

        <div class="<?= ($action == 'update') ? 'col-md-6' : '' ?>">
                <div class="panel panel-default">
                        <div class="panel-heading">
                                Général
                        </div>
                        <div class="panel-body">
                                <?php $form = ActiveForm::begin(); ?>
                                <?= Html::hiddenInput('classDocument',$model->getClass(), ['id' => 'class-document']) ?>
                                <?= Html::hiddenInput('typeAction',$action, ['id' => 'type-action']) ?>
                                <?php if ($action == 'update'): ?>
                                        <?= Html::hiddenInput('idDocument',$model->id, ['id' => 'id-document']) ?>
                                <?php endif; ?>
                                <?= $form->field($model, 'name')->label('Nom du document') ?>

                                <?php if($action == 'update'): ?>
                                        <?= $form->field($model, 'id_user', [
                                                'template' => '{label} <div>{input}</div>'.$model->user->getUsername(),
                                        ])->hiddenInput(); ?>
                                <?php else: ?>
                                        <?php
                                        $usersArray = User::findBy()->orderBy('type DESC')->all();
                                        ?>
                                        <?= $form->field($model, 'id_user', [
                                                'template' => '{label} <a href="' . Yii::$app->urlManager->createUrl(['user/create']) . '" class="btn btn-xs btn-default">Nouvel utilisateur <span class="glyphicon glyphicon-plus"></span></a><div>{input}</div>{hint}',
                                        ])
                                                ->dropDownList(
                                                        ArrayHelper::map($usersArray, 'user_id', function ($model) {
                                                                if(isset($model['name_legal_person']) && strlen($model['name_legal_person'])) {
                                                                        return 'Personne morale / '.$model['name_legal_person'] ;
                                                                }
                                                                else {
                                                                        return $model['lastname'] . ' ' . $model['name'];
                                                                }
                                                        }),
                                                        [
                                                                '@change' => 'changeUser',
                                                                'prompt' => '--',
                                                                'v-model' => 'idUser',
                                                        ]
                                                ); ?>
                                <?php endif; ?>


                                <?= $form->field($model, 'address')->textarea(['rows' => 2, 'v-model' => 'document.address']) ?>
                                <?php if ($action == 'update'): ?>
                                        <?= $form->field($model, 'comment')->textarea(['rows' => 2])->hint('Affiché en bas de la facture') ?>
                                <?php endif; ?>

                                <?php if($model->getClass() == 'Invoice'): ?>
                                        <template v-if="idUser > 0">
                                                <strong>Bons de livraison</strong>
                                                <table v-if="deliveryNotes && deliveryNotes.length > 0" class="table table-bordered">
                                                        <thead>
                                                                <tr>
                                                                        <?php if($action == 'create'): ?><th></th><?php endif; ?>
                                                                        <th>Libellé</th>
                                                                        <th v-if="taxRateProducer != 0">Montant (TTC)</th>
                                                                        <th v-else>Montant</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                <tr v-for="deliveryNote in deliveryNotes">
                                                                        <?php if($action == 'create'): ?>
                                                                                <td><input type="checkbox" name="Invoice[deliveryNotes][]" :value="deliveryNote.id" /></td>
                                                                        <?php endif; ?>
                                                                        <td>{{ deliveryNote.name }}</td>
                                                                        <td>{{ formatPrice(deliveryNote.total) }}</td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                                <div v-else class="alert alert-warning">Aucun bon de livraison pour cet utilisateur.</div>
                                        </template>
                                <?php endif; ?>

                                <div class="form-group">
                                        <?= Html::submitButton($model->isNewRecord ? 'Ajouter' : 'Modifier', ['class' => 'btn btn-primary']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                        </div>
                </div>
        </div>

        <?php if ($action == 'update'): ?>

                <div class="col-md-6">
                        <div id="" class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-sticky-note-o"></i></span>
                                <div class="info-box-content">
                                        <span class="info-box-text"><?= $typeDocument ?> <span v-html="document.html_label"></span></span>
                                        <span class="info-box-number">{{ document.reference }}</span>
                                        <span class="info-box-text">Date</span>
                                        <span class="info-box-number">{{ document.date }}</span>
                                </div>
                        </div>
                        <div id="" class="info-box">
                                <span class="info-box-icon bg-yellow"><i class="fa fa-euro"></i></span>
                                <div class="info-box-content">
                                        <span class="info-box-text">Total<span v-if="taxRateProducer != 0"> (TTC)</span></span>
                                        <span class="info-box-number">{{ formatPrice(total_with_tax) }}</span>
                                </div>
                        </div>
                        <div id="" class="info-box">
                                <span class="info-box-icon bg-blue"><i class="fa fa-download"></i></span>
                                <div class="info-box-content">
                                        <a href="<?= Yii::$app->urlManager->createUrl([Yii::$app->controller->getControllerUrl().'/download', 'id' => $model->id]) ?>" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span> Télécharger (PDF)</a>
                                </div>
                        </div>
                        <div v-if="document.status == 'draft'" id=""  class="info-box">
                                <span class="info-box-icon bg-red"><i class="fa fa-flash"></i></span>
                                <div class="info-box-content">
                                        <form action="<?= Yii::$app->urlManager->createUrl([Yii::$app->controller->getControllerUrl().'/validate']) ?>">
                                                <?= Html::hiddenInput('id', $model->id); ?>
                                                <button class="btn btn-default"><span class="glyphicon glyphicon-ok"></span> Valider le document</button>
                                        </form>
                                        <?php if(isset($model->user) && strlen($model->user->email) > 0): ?>
                                                <a href="<?= Yii::$app->urlManager->createUrl([Yii::$app->controller->getControllerUrl().'/send', 'id' => $model->id]) ?>" class="btn btn-default"><span class="glyphicon glyphicon-send"></span> Envoyer le document</a>
                                        <?php endif; ?>
                                </div>
                        </div>
                </div>
                <div class="clr"></div>

                <div class="">
                        <div class="panel panel-default" id="block-add-product">
                                <div class="panel-heading">
                                        Ajouter un produit
                                </div>
                                <div class="panel-body">
                                        <div class="col-md-3">
                                                <strong>Produit</strong>
                                                <select class="form-control" v-model="productAddId"
                                                        @change="changeProductAdd">
                                                        <option value="0" selected="selected">--</option>
                                                        <option v-for="product in productsArray" :value="product.id">
                                                                {{ product.name }}
                                                        </option>
                                                </select>
                                        </div>
                                        <template v-if="productAddId > 0">
                                                <div class="col-md-3">
                                                        <strong>Prix unitaire</strong>
                                                        <div class="input-group">
                                                                <input type="text" class="form-control input-price"
                                                                       v-model="productAddPrice" @change="formatProductAddPrice"/>
                                                                <span class="input-group-addon"><span
                                                                            class="glyphicon glyphicon-euro"></span> <span v-if="taxRateProducer != 0">TTC</span></span>
                                                        </div>
                                                </div>
                                                <div class="col-md-3">
                                                        <strong>Quantité</strong>
                                                        <div class="input-group input-group-quantity">
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-default" type="button"
                                                                            @click="changeQuantityProductAdd(-1)">-</button>
                                                                </span>
                                                                <input type="text" class="form-control input-quantity"
                                                                       v-model="productAddQuantity" @change="formatProductAddQuantity"/>
                                                                <span class="input-group-addon">{{ productsArray[productAddId].wording_unit }}</span>
                                                                <span class="input-group-btn">
                                                                            <button class="btn btn-default"
                                                                                    type="button"
                                                                                    @click="changeQuantityProductAdd(1)">+</button>
                                                                </span>
                                                        </div>
                                                </div>
                                                <div class="col-md-3 total">
                                                        <strong>Total</strong>
                                                        <div class="input-group">
                                                                <input type="text" class="form-control input-price" readonly
                                                                       :value="formatPrice(productAddPrice * productAddQuantity)"/>
                                                                <span class="input-group-addon"><span
                                                                            class="glyphicon glyphicon-euro"></span> <span v-if="taxRateProducer != 0">TTC</span></span>
                                                        </div>
                                                        <button class="btn btn-primary" value="Ajouter"
                                                                @click="submitProductAdd">Ajouter
                                                        </button>
                                                        <div class="clr"></div>
                                                </div>
                                        </template>
                                        <div class="clr"></div>
                                </div>
                        </div>
                        <div class="panel panel-default">
                                <div class="panel-heading">
                                        Produits
                                </div>
                                <div class="panel-body">
                                        <div id="block-list-products">
                                                <table class="table table-bordered" v-if="total > 0">
                                                        <thead>
                                                        <tr>
                                                                <th>Nom</th>
                                                                <th>Prix (unité)</th>
                                                                <th>Quantité</th>
                                                                <th v-if="taxRateProducer != 0">TVA</th>
                                                                <th v-if="taxRateProducer != 0">Total HT</th>
                                                                <th v-else>Total</th>
                                                                <th>Supprimer</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <template v-for="order in ordersArray">
                                                                <tr v-for="productOrder in order.productOrder">
                                                                        <td class="col-md-4">
                                                                                <div class="product-name">{{ productsArray[productOrder.id_product].name }}</div>
                                                                                <ul class="product-order-meta">
                                                                                        <li>{{ order.username }}</li>
                                                                                        <li v-if="order.distribution_date">{{ order.distribution_date }}</li>
                                                                                        <li v-if="order.point_sale_name">{{ order.point_sale_name }}</li>
                                                                                </ul>
                                                                        </td>
                                                                        <td class="col-md-2">
                                                                            {{ formatPrice(getProductOrderPrice(productOrder)) }}
                                                                        </td>
                                                                        <td class="col-md-2">{{ productOrder.quantity }}</td>
                                                                        <td class="col-md-1" v-if="taxRateProducer != 0">
                                                                                {{ productsArray[productOrder.id_product].tax_rate * 100 }} %
                                                                        </td>
                                                                        <td class="col-md-2">
                                                                            {{ formatPrice(productOrder.quantity * getProductOrderPrice(productOrder)) }}
                                                                        </td>
                                                                        <td class="col-md-1">
                                                                                <a class="btn btn-default" @click="deleteProductOrder(productOrder.id)">
                                                                                        <span class="glyphicon glyphicon-trash"></span>
                                                                                </a>
                                                                        </td>
                                                                </tr>
                                                        </template>
                                                        <template v-if="taxRateProducer != 0">
                                                                <tr>
                                                                        <td colspan="4"><strong>Total HT</strong></td>
                                                                        <td><strong>{{ formatPrice(total) }} HT</strong></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr>
                                                                        <td colspan="4"><strong>Montant TVA</strong></td>
                                                                        <td><strong>{{ formatPrice(total_with_tax - total) }}</strong></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr>
                                                                        <td colspan="4"><strong>Total TTC</strong></td>
                                                                        <td><strong>{{ formatPrice(total_with_tax) }} TTC</strong></td>
                                                                        <td></td>
                                                                </tr>
                                                        </template>
                                                        <template v-else>
                                                                <tr>
                                                                        <td colspan="3"><strong>Total</strong></td>
                                                                        <td><strong>{{ formatPrice(total) }}</strong></td>
                                                                        <td></td>
                                                                </tr>
                                                        </template>

                                                        </tbody>
                                                </table>
                                                <div v-else class="alert alert-info">
                                                        Aucun produit.
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        <?php endif; ?>


</div>
