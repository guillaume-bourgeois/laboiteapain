<?php

$displayPrices = Yii::$app->controller->getClass() != 'DeliveryNote' || (Yii::$app->controller->getClass() == 'DeliveryNote' && Producer::getConfig('document_display_prices_delivery_note')) ;

?>

<div class="document-download">

        <div id="block-addresses">
                <div class="producer">
                        <?php if(strlen($producer->logo)) : ?>
                                <div class="logo">
                                        <img style="max-height: 80px;" src="<?= $producer->getUrlLogo() ?>" />
                                </div>
                        <?php endif; ?>
                        <div class="address"><?= $producer->getFullAddress(true) ; ?></div>
                </div>
                <div class="user">
                        <?= $document->user->getFullAddress(true) ; ?>
                </div>
        </div>

        <div id="block-infos-document">
                <div class="date">
                        Le <?= strftime('%d %B %Y', strtotime($document->date)) ?>
                </div>
                <div class="reference">
                        <?php if(strlen($document->reference)) : ?>
                                <?= $document->getType(); ?> N°<?= $document->reference ; ?>
                        <?php else: ?>
                                <div class="block-is-draft"><?= $document->getType(); ?> non validé<?= ($document->getType() == 'Facture') ? 'e' : '' ?></div>
                        <?php endif; ?>
                </div>
                <div class="name">
                        <strong>Libellé : </strong><?= $document->name ; ?>
                </div>
        </div>

        <div id="block-products">
                <?php if(count($document->orders) > 0) : ?>
                <table class="table table-bordered">
                        <thead>
                                <tr>
                                        <th class="align-left">Produit</th>
                                        <?php if($displayPrices): ?>
                                                <?php if(GlobalParam::getCurrentProducer()->taxRate->value == 0): ?>
                                                        <th>Prix unitaire</th>
                                                <?php else: ?>
                                                        <th>Prix unitaire HT</th>
                                                <?php endif; ?>
                                        <?php endif; ?>
                                        <th>Quantité</th>
                                        <th>Unité</th>
                                        <?php if($displayPrices): ?>
                                                <?php if(GlobalParam::getCurrentProducer()->taxRate->value == 0): ?>
                                                        <th>Prix</th>
                                                <?php else: ?>
                                                        <th>TVA</th>
                                                        <th>Prix HT</th>
                                                <?php endif; ?>
                                        <?php endif; ?>

                                </tr>
                        </thead>
                        <tbody>

                        <?php if($document->isDisplayOrders()): ?>
                                <?php foreach($document->orders as $order): ?>
                                        <tr>
                                                <td>
                                                        <strong><?= Html::encode($order->getUsername()) ; ?></strong>
                                                        le <?= date('d/m/Y', strtotime($order->distribution->date)) ?>
                                                </td>
                                                <?php if($displayPrices): ?>
                                                        <td class="align-center"></td>
                                                <?php endif; ?>
                                                <td></td>
                                                <td></td>
                                                <?php if($displayPrices): ?>
                                                        <?php if(GlobalParam::getCurrentProducer()->taxRate->value != 0): ?>
                                                                <td class="align-center"></td>
                                                        <?php endif; ?>
                                                        <td class="align-center"></td>
                                                <?php endif; ?>
                                        </tr>
                                        <?php foreach($order->productOrder as $productOrder): ?>
                                                <?= $this->render('_download_product_line', [
                                                        'document' => $document,
                                                        'productOrder' => $productOrder,
                                                        'displayOrders' => true,
                                                        'displayPrices' => $displayPrices
                                                ]) ?>
                                        <?php endforeach; ?>
                                <?php endforeach; ?>
                        <?php else: ?>
                                <?php foreach($document->getProductsOrders() as $product): ?>
                                        <?php foreach($product as $productOrder): ?>
                                                <?= $this->render('_download_product_line', [
                                                        'document' => $document,
                                                        'productOrder' => $productOrder,
                                                        'displayPrices' => $displayPrices
                                                ]) ?>
                                        <?php endforeach; ?>
                                <?php endforeach; ?>
                        <?php endif; ?>
                                <?php if($displayPrices): ?>
                                        <?php $typeAmount = $document->isInvoicePrice() ? Order::INVOICE_AMOUNT_TOTAL : Order::AMOUNT_TOTAL ; ?>

                                        <?php if(GlobalParam::getCurrentProducer()->taxRate->value != 0): ?>

                                        <tr>
                                                <td class="align-right" colspan="5"><strong>Total HT</strong></td>
                                                <td class="align-center">
                                                        <?= Price::format($document->getAmount($typeAmount)); ?>
                                                </td>
                                        </tr>
                                        <tr>
                                                <td class="align-right" colspan="5"><strong>TVA</strong></td>
                                                <td class="align-center"><?= Price::format($document->getAmountWithTax($typeAmount) - $document->getAmount($typeAmount)) ?></td>
                                        </tr>
                                        <tr>
                                                <td class="align-right" colspan="5"><strong>Total TTC</strong></td>
                                                <td class="align-center"><?= Price::format($document->getAmountWithTax($typeAmount)) ?></td>
                                        </tr>
                                        <?php else: ?>
                                                <tr>
                                                        <td class="align-right" colspan="4">
                                                                <strong>Total</strong><br />
                                                                TVA non applicable
                                                        </td>
                                                        <td class="align-center"><?= Price::format($document->getAmount($typeAmount)) ?></td>
                                                </tr>
                                        <?php endif; ?>
                                <?php endif; ?>
                        </tbody>
                </table>
                <?php else : ?>
                        <div id="block-no-product">
                                <strong>Aucun produit</strong>
                        </div>
                <?php endif; ?>
        </div>

        <?php if(strlen($document->comment)): ?>
                <div class="block-infos">
                        <strong>Commentaire</strong><br />
                        <?= Html::encode($document->comment) ?>
                </div>
        <?php endif; ?>

        <?php
                $fieldProducerDocumentInfo = 'document_infos_'.str_replace('deliverynote','delivery_note',strtolower($document->getClass())) ; ?>
        <?php if(strlen($producer->$fieldProducerDocumentInfo)): ?>
        <div class="block-infos">
                <strong>Informations</strong><br />
                <?= nl2br(Html::encode($producer->$fieldProducerDocumentInfo)) ?>
        </div>
        <?php endif; ?>
</div>