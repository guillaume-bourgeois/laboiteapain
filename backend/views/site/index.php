<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html ;

$this->title = 'Tableau de bord';

?>
<div class="site-index">

    <?php if(!$producer->getFreePrice()): ?>
        <div class="callout callout-info">
            <h4>Abonnement à prix libre</h4>
            <p><i>distrib</i> fonctionne avec système d'abonnement à prix libre pour en
                assurer la plus grande diffusion possible. Ceci correspond également à l'unique source de revenus 
                nécessaire à la maintenance et au développement de la plateforme.<br />
                Si cet outil est important dans votre activité, nous vous encourageons à nous soutenir en vous abonnant.
            </p>
            <p><?= Html::a('&gt; Définir le montant de mon abonnement', ['producer/billing'], ['class' => 'btn btn-default']); ?></p>
        </div>
    <?php endif; ?>
    
    <?php if(Yii::$app->request->get('error_products_points_sale')): ?>
        <div class="alert alert-warning">
            Vous devez ajouter <?php if(!$productsCount): ?> des produits<?php endif; ?>
            <?php if(!$productsCount && !$pointsSaleCount): ?> et<?php endif; ?>
            <?php if(!$pointsSaleCount): ?> un ou des points de vente <?php endif; ?>
            avant d'effectuer cette action.
        </div>
    <?php endif; ?>
    
    <?php if(!$productsCount): ?>
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-clone"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><br /><?= Html::a('Ajouter des produits', ['product/create'], ['class' => 'btn btn-default']); ?></span>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if(!$pointsSaleCount): ?>
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-map-marker"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><br /><?= Html::a('Ajouter des points de vente', ['point-sale/create'], ['class' => 'btn btn-default']); ?></span>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if($productsCount && $pointsSaleCount && !count($distributionsArray)): ?>
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-calendar"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><br /><?= Html::a('Ajouter des jours de distribution', ['distribution/index'], ['class' => 'btn btn-default']); ?></span>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if(count($distributionsArray)): ?>
    <div id="distributions">
        <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                Prochaines distributions
              </h3>
            </div>
            <div class="panel-body">
                <!-- distributions -->
                <?php foreach($distributionsArray as $distribution): ?>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green date">
                                <span class="day"><?= strftime('%A', strtotime($distribution->date)) ?></span>
                                <span class="num"><?= date('d', strtotime($distribution->date)) ?></span>
                                <span class="month"><?= strftime('%B', strtotime($distribution->date)) ?></span>
                            </span>
                            <div class="info-box-content">
                                <span class="info-box-text">
                                    <?php if(count($distribution->order)): ?>
                                        <strong><?= count($distribution->order); ?></strong> COMMANDES
                                    <?php else: ?>
                                        AUCUNE COMMANDE
                                    <?php endif; ?>
                                </span>
                                <span class="info-box-number"></span>
                                <div class="buttons">
                                    <?= Html::a('<span class="fa fa-eye"></span>', ['distribution/index', 'date' => $distribution->date], ['class' => 'btn btn-default']); ?>
                                    <?php if(count($distribution->order)): ?><?= Html::a('<span class="fa fa-download"></span>', ['distribution/report', 'date' => $distribution->date], ['class' => 'btn btn-default']); ?><?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
    
    <div class="clr"></div>
    
    <!-- dernières commandes -->
    <?php if(is_array($ordersArray) && count($ordersArray)): ?>
    <div id="last-orders" class="">
        <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                Commandes à venir
              </h3>
            </div>
            <div class="panel-body">
                    <table class="table table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Date</th>
                                <th>Client</th>
                                <th>Produits</th>
                                <th>Point de vente</th>
                                <th>Montant</th>
                                <th>Historique</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($ordersArray as $order): ?>
                            <tr class="<?= $order->getClassHistory() ; ?>">
                                <td class="infos"><?= $order->getStrOrigin(true); ?></td>
                                <td class="date">
                                    <div class="block-date">
                                        <div class="day"><?= strftime('%A', strtotime($order->distribution->date)) ?></div>
                                        <div class="num"><?= date('d', strtotime($order->distribution->date)) ?></div>
                                        <div class="month"><?= strftime('%B', strtotime($order->distribution->date)) ?></div>
                                    </div>
                                </td>
                                <td>
                                    <?= $order->getStrUser(); ?><br />
                                    <?php if(strlen($order->comment)): ?>
                                        <div class="comment"><span class="glyphicon glyphicon-comment"></span> <?= nl2br(Html::encode($order->comment)) ; ?></div>
                                    <?php endif; ?>
                                </td>
                                <td><?= $order->getCartSummary() ; ?></td>
                                <td><?= $order->getPointSaleSummary() ; ?></td>
                                <td><?= $order->getAmountWithTax(Order::AMOUNT_TOTAL, true) ; ?></td>
                                <td class="history"><?= $order->getStrHistory() ; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
