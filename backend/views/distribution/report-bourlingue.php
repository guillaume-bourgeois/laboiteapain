<?php 

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use common\models\Order ;
use common\models\Product ;

$dayWeek = date('w', strtotime($date));
$dayWeekArray = [0 => 'sunday', 1 => 'monday', 2 => 'tuesday', 3 => 'wednesday', 4 => 'thursday', 5 => 'friday', 6 => 'saturday'];
$fieldInfosPointSale = 'infos_' . $dayWeekArray[$dayWeek];

$html = '' ;

$count = count($productsArray) ;
$limit = 100 ;
$isBig = $count > $limit ;

// par point de vente
foreach ($pointsSaleArray as $pointSale) {
    if (count($pointSale->orders)) {
        
        $html .= '<h3>'.$pointSale->name.'</h3>' ;
        
        $html .= '<table class="">'
            . '<thead>'
                . '<tr>'
                    . '<th>Nom / prénom</th>'
                    . '<th>Téléphone</th>'
                    . '<th>Mail</th>'
                    . '<th>Produits</th>'
                    . ($isBig ? '<th>Produits</th>' : '')
                    . '<th>Commentaire</th>'
                    . '<th>Paiement</th>'
                    . '<th>Moyen de paiement</th>'
                    . '<th>Montant</th>'
                . '</tr>'
            . '<tbody>';
        
                foreach ($pointSale->orders as $order) {
                    $html .= '<tr>' ;
                    $strUser = '';

                    // username
                    $strUser = $order->getStrUser() ;
                    
                    if(strlen($order->comment_point_sale)) 
                    {
                        $strUser .= '<br /><em>'.$order->comment_point_sale.'</em>' ;
                    }

                    $html .= '<td>'.$strUser.'</td>';

                    // téléphone
                    $html .= '<td>' ;
                    if (isset($order->user) && strlen($order->user->phone)) {
                        $html .= $order->user->phone ;
                    }
                    $html .= '</td>' ;

                    // mail
                    $html .= '<td>' ;
                    if (isset($order->user) && strlen($order->user->email)) {
                        $html .= $order->user->email ;
                    }
                    $html .= '</td>' ;

                    // produits
                    $strProducts = '';
                    foreach ($productsArray as $product) {
                        $add = false;
                        foreach ($order->productOrder as $productOrder) {
                            if($product->id == $productOrder->id_product) {
                                $unit = (Product::strUnit($productOrder->unit, 'wording_short', true) == 'p.') ? '' : '&nbsp;'.Product::strUnit($productOrder->unit, 'wording_short', true) ;
                                $strProducts .= $product->name . ' (' .$productOrder->quantity .$unit.')<br />';
                                $add = true;
                            }
                        }
                    }
                    
                    $html .= '<td>'.substr($strProducts, 0, strlen($strProducts) - 6).'</td>';
                    if($isBig) {
                        $html .= '<td></td>' ;
                    }

                    // commentaire
                    $html .= '<td>'.$order->comment.'</td>';

                    // paiement
                    $html .= '<td>' ;
                    if(isset($order->mean_payment) && strlen($order->mean_payment) > 0)
                    {
                        $html .= 'Payé' ;
                    }
                    $html .= '</td>' ;

                    // moyen de paiement
                    $html .= '<td>' ;
                    if(isset($order->mean_payment) && strlen($order->mean_payment) > 0)
                    {
                        if($order->mean_payment == 'cheque') {
                            $html .= 'Chèque' ;
                        }
                        elseif($order->mean_payment == 'money') {
                            $html .= 'Espèces' ;
                        }
                        elseif($order->mean_payment == 'credit') {
                            $html .= 'Crédit' ;
                        }
                        elseif($order->mean_payment == 'credit-card') {
                            $html .= 'Carte bancaire' ;
                        }
                        elseif($order->mean_payment == 'transfer') {
                            $html .= 'Virement' ;
                        }
                        else {
                            $html .= $order->mean_payment ;
                        }
                    }
                    $html .= '</td>' ;

                    if($pointSale->credit) {
                        $credit = '' ;

                        if(isset($order->user) && $order->user->id) {
                            $userProducer = UserProducer::searchOne([
                                    'id_user' => $order->user->id
                            ]);

                            if($userProducer) {
                                $credit = number_format($userProducer->credit,2).' €' ;
                            }
                        }

                        $html .= '<td>'.$credit.'</td>' ;       
                    }
                    
                    $html .= '<td><strong>'.number_format($order->amount_with_tax, 2) . ' € ';
                    
                    $html .= '</strong></td>' ;
                    
                    $html .= '</tr>' ;
                    
                }

                $html .= '<tr><td><strong>Total</strong></td><td></td><td></td>' ;
                
                $strProducts = '';
                $cpt = 0 ;
                foreach ($productsArray as $product) {
                    foreach(Product::$unitsArray as $unit => $dataUnit) {
                        $quantity = Order::getProductQuantity($product->id, $pointSale->orders, false, $unit);
                        if ($quantity) {
                            $theUnit = (Product::strUnit($unit, 'wording_short', true) == 'p.') ? '' : '&nbsp;'.Product::strUnit($unit, 'wording_short', true) ;
                            $strProducts .= $product->name . ' (' .$quantity .$theUnit.')<br />';
                        }
                    }

                    if($isBig && $cpt == $limit) {
                        $strProducts .= '</td><td>' ;
                    }

                    $cpt ++ ;
                }
                
                $strProducts = substr($strProducts, 0, strlen($strProducts) - 6) ;
                
                $html .= '<td>'.$strProducts.'</td><td></td><td></td><td></td>' ;
                if($pointSale->credit) {
                    $html .= '<td></td>' ;
                }
                $html .= '<td><strong>'.Price::format($pointSale->revenues_with_tax) . '</strong></td>';
                
                $html .= '</tbody></table><pagebreak>' ;
    }
}

// par point de vente

$html .= '<h3>Points de vente</h3>' ;
$html .= '<table class="">'
            . '<thead>'
                . '<tr>'
                    . '<th>Point de vente</th>'
                    . '<th>Produits</th>'
                    . ( $isBig ? '<th>Produits</th>' : '')
                    . '<th>Montant</th>'
                . '</tr>'
            . '<tbody>';

$revenues = 0 ;
foreach ($pointsSaleArray as $pointSale) 
{
    if (count($pointSale->orders)) 
    {
        $html .= '<tr><td>'.$pointSale->name.'</td><td>' ;

        $cpt = 0 ;
        foreach ($productsArray as $product) {
            foreach(Product::$unitsArray as $unit => $dataUnit) {
                $quantity = Order::getProductQuantity($product->id, $pointSale->orders, false, $unit);
                if ($quantity) {
                    $theUnit = (Product::strUnit($unit, 'wording_short', true) == 'p.') ? '' : '&nbsp;'.Product::strUnit($unit, 'wording_short', true) ;
                    $html .= $product->name . ' (' .$quantity .$theUnit.')<br />';
                }
            }

            if($isBig && $cpt == $limit) {
                $html .= '</td><td>' ;
            }

            $cpt ++ ;
        }
        
        $html = substr($html, 0, strlen($html) - 6) ;
        
        $html .= '</td><td>'.Price::format($pointSale->revenues_with_tax, 2).'</td></tr>' ;
        $revenues += $pointSale->revenues_with_tax ;
    }
}

// total
$html .= '<tr><td><strong>Total</strong></td><td>' ;

$cpt = 0 ;
foreach ($productsArray as $product) {
    foreach(Product::$unitsArray as $unit => $dataUnit) {
        $quantity = Order::getProductQuantity($product->id, $ordersArray, false, $unit);
        if ($quantity) {
            $theUnit = (Product::strUnit($unit, 'wording_short', true) == 'p.') ? '' : '&nbsp;'.Product::strUnit($unit, 'wording_short', true) ;
            $html .= $product->name . ' (' .$quantity .$theUnit.')<br />';
        }
    }
    if($isBig && $cpt == $limit) {
        $html .= '</td><td>' ;
    }

    $cpt ++ ;
}

$html = substr($html, 0, strlen($html) - 6) ;

$html .= '</td><td><strong>'.number_format($revenues, 2).' €</strong></td></tr>' ;

$html .= '</tbody></table>' ;

echo $html ;

?>
