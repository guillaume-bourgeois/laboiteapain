<?php 

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use common\models\Order ;

$dayWeek = date('w', strtotime($date));
$dayWeekArray = [0 => 'sunday', 1 => 'monday', 2 => 'tuesday', 3 => 'wednesday', 4 => 'thursday', 5 => 'friday', 6 => 'saturday'];
$fieldInfosPointSale = 'infos_' . $dayWeekArray[$dayWeek];

$html = '' ;

// par point de vente
foreach ($pointsSaleArray as $pointSale) {
    if (count($pointSale->orders) && strlen($pointSale->$fieldInfosPointSale)) {
        
        $html .= '<h3>'.$pointSale->name.'</h3>' ;
        $colCredit = ($pointSale->credit) ? '<th>Rappel crédit</th>' : '' ;
        
        $html .= '<table class="table table-bordered">'
            . '<thead>'
                . '<tr>'
                    . '<th>Client</th>'
                    . '<th>Produits</th>'
                    . '<th>Commentaire</th>' 
                    . $colCredit
                    . '<th>Montant</th>'
                . '</tr>'
            . '<tbody>';
        
                foreach ($pointSale->orders as $order) {
                    $html .= '<tr>' ;
                    $strUser = '';

                    // username
                    if ($order->user) {
                        $strUser = $order->user->name . " " . $order->user->lastname; 
                    } else {
                        $strUser = $order->username; 
                    }
                    
                    if(strlen($order->comment_point_sale)) 
                    {
                        $strUser .= '<br /><em>'.$order->comment_point_sale.'</em>' ;
                    }
                    
                    // téléphone
                    if (isset($order->user) && strlen($order->user->phone)) {
                        $strUser .= '<br />' . $order->user->phone . '';
                    }

                    $html .= '<td>'.$strUser.'</td>';
                    
                    // produits
                    $strProducts = '';
                    foreach ($productsArray as $product) {
                        $add = false;
                        foreach ($order->productOrder as $productOrder) {
                            if ($product->id == $productOrder->id_product) {
                                $strProducts .= $productOrder->quantity . '&nbsp;' . $product->name . ', ';
                                $add = true;
                            }
                        }
                    }
                    
                    $html .= '<td>'.substr($strProducts, 0, strlen($strProducts) - 2).'</td>';
                    $html .= '<td>'.$order->comment.'</td>';
                    
                    if($pointSale->credit) {
                        $credit = '' ;
                        if(isset($order->user) && isset($order->user->userProducer)) {
                            $credit = number_format($order->user->userProducer[0]->credit,2).' €' ;
                        }
                        $html .= '<td>'.$credit.'</td>' ;       
                    }
                    
                    $html .= '<td><strong>'.number_format($order->amount, 2) . ' € ';

                    if($order->getPaymentStatus() == Order::PAYMENT_PAID)
                    {
                        $html .= '(payé)' ;
                    }
                    elseif($order->getPaymentStatus() == Order::PAYMENT_UNPAID && $order->getAmount(Order::AMOUNT_PAID))
                    {
                        $html .= '(reste '.$order->getAmount(Order::AMOUNT_REMAINING, true).' à payer)' ;
                    }
                    elseif($order->getPaymentStatus() == Order::PAYMENT_SURPLUS)
                    {
                        $html .= '(surplus : '.$order->getAmount(Order::PAYMENT_SURPLUS, true).' à rembourser)' ;
                    }
                    
                    $html .= '</strong></td>' ;
                    
                    $html .= '</tr>' ;
                    
                }

                $html .= '<tr><td><strong>Total</strong></td>' ;
                
                $strProducts = '';
                foreach ($productsArray as $product) {
                    $quantity = Order::getProductQuantity($product->id, $pointSale->orders);
                    $strQuantity = '';
                    if ($quantity) {
                        $strQuantity = $quantity;
                        $strProducts .= $strQuantity .'&nbsp;'. $product->name . ', ';
                    }
                }
                
                $strProducts = substr($strProducts, 0, strlen($strProducts) - 2) ;
                
                $html .= '<td>'.$strProducts.'</td><td></td>' ;
                if($pointSale->credit) {
                    $html .= '<td></td>' ;
                }
                $html .= '<td><strong>'.number_format($pointSale->revenues, 2) . ' €</strong></td>';
                
                $html .= '</tbody></table><pagebreak>' ;
    }
}

// par point de vente
$html .= '<h3>Points de vente</h3>' ;
$html .= '<table class="table table-bordered">'
            . '<thead>'
                . '<tr>'
                    . '<th>Point de vente</th>'
                    . '<th>Produits</th>'
                    . '<th>Montant</th>'
                . '</tr>'
            . '<tbody>';

$revenues = 0 ;
foreach ($pointsSaleArray as $pointSale) 
{
    if (count($pointSale->orders) && strlen($pointSale->$fieldInfosPointSale)) 
    {
        $html .= '<tr><td>'.$pointSale->name.'</td><td>' ;
        foreach ($productsArray as $product) {
            $quantity = Order::getProductQuantity($product->id, $pointSale->orders);
            $strQuantity = ($quantity) ? $quantity : '' ;
                
            if(strlen($strQuantity)) {
                $html .= $strQuantity . '&nbsp;'.$product->name.', ' ;
            }
                
        }
        
        $html = substr($html, 0, strlen($html) - 2) ;
        
        $html .= '</td><td>'.number_format($pointSale->revenues, 2).' €</td></tr>' ;
        $revenues += $pointSale->revenues ;
    }
}

// total
$html .= '<tr><td><strong>Total</strong></td><td>' ;
foreach ($productsArray as $product) {
    $quantity = Order::getProductQuantity($product->id, $ordersArray);
    if($quantity) {
         $html .= $quantity . '&nbsp;'.$product->name.', ' ;
    }
}

$html = substr($html, 0, strlen($html) - 2) ;

$html .= '</td><td><strong>'.number_format($revenues, 2).' €</strong></td></tr>' ;

$html .= '</tbody></table>' ;

echo $html ;

?>
