<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html ;
use yii\widgets\ActiveForm;

$this->setTitle('Envoyer un email') ;
$this->addBreadcrumb(['label' => 'Communiquer', 'url' => ['user/index']]) ;
$this->addBreadcrumb($this->getTitle()) ;

?>

<div class="submenu">
    <a class="btn <?php if($section == 'producers'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" href="<?= Yii::$app->urlManager->createUrl(['communicate-admin/index', 'section' => 'producers']); ?>">
        <span class="glyphicon glyphicon-grain"></span> Producteurs <span class="glyphicon glyphicon-triangle-bottom"></span>
    </a>
    <a class="btn <?php if($section == 'users'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>" href="<?= Yii::$app->urlManager->createUrl(['communicate-admin/index', 'section' => 'users']); ?>">
        <span class="glyphicon glyphicon-user"></span> Utilisateurs <span class="glyphicon glyphicon-triangle-bottom"></span>
    </a>
</div>

<div id="">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Envoyer un email</h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($mailForm, 'subject')->textInput() ; ?>
                <?= $form->field($mailForm, 'message')->textarea(['rows' => '15']) ; ?>
                <div class="form-group">
                    <?= Html::submitButton( 'Envoyer', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des emails <span class="label label-default"><?= count($usersArray); ?></span></h3>
            </div>
            <div class="panel-body">
                <?= implode(', ', $usersArray); ?>
            </div>
        </div>
    </div>
    <div class="clr"></div>
</div>
