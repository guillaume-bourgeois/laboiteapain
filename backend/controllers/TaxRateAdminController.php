<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace backend\controllers;

use common\models\TaxRate;
use Yii;
use common\models\User;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\Upload;
use common\models\Producer;
use yii\data\ActiveDataProvider;
use common\models\Invoice;

/**
 * TaxRateAdminController implements the CRUD actions for TaxRate model.
 */
class TaxRateAdminController extends BackendController
{

        public function behaviors()
        {
                return [
                        'verbs' => [
                                'class' => VerbFilter::className(),
                                'actions' => [
                                        'delete' => ['post'],
                                ],
                        ],
                        'access' => [
                                'class' => AccessControl::className(),
                                'rules' => [
                                        [
                                                'allow' => true,
                                                'roles' => ['@'],
                                                'matchCallback' => function ($rule, $action) {
                                                        return User::getCurrentStatus() == USER::STATUS_ADMIN;
                                                }
                                        ]
                                ],
                        ],
                ];
        }

        /**
         * Liste les taxes.
         *
         * @return mixed
         */
        public function actionIndex()
        {
                $dataProviderTaxRate = new ActiveDataProvider([
                        'query' => TaxRate::find()
                ]);

                return $this->render('index', [
                        'dataProviderTaxRate' => $dataProviderTaxRate,
                ]);
        }

        /**
         * Crée une taxe.
         *
         * @return mixed
         */
        public function actionCreate()
        {
                $model = new TaxRate();

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        Yii::$app->getSession()->setFlash('success', 'Taxe créé.');
                        return $this->redirect(['index']);
                } else {
                        return $this->render('create', [
                                'model' => $model,
                        ]);
                }
        }

        /**
         * Édition d'une taxe.
         *
         * @return mixed
         */
        public function actionUpdate($id)
        {


                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        Yii::$app->getSession()->setFlash('success', 'Taxe édité.');
                        return $this->redirect(['index']);
                } else {
                        return $this->render('update', [
                                'model' => $model,
                        ]);
                }
        }

        /**
         * Supprime une commande récurrente.
         *
         * @param integer $id
         */
        public function actionDelete($id)
        {
                $taxeRate = TaxRate::searchOne([
                        'id' => $id
                ]) ;
                $taxeRate->delete();

                Yii::$app->getSession()->setFlash('success', 'Taxe supprimé');
                return $this->redirect(['tax-rate-admin/index']);
        }


        /**
         * Finds the Developpement model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return Developpement the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
                if (($model = TaxRate::findOne($id)) !== null) {
                        return $model;
                } else {
                        throw new NotFoundHttpException('The requested page does not exist.');
                }
        }

}
