<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use common\helpers\GlobalParam;
use Yii;
use common\models\User;
use common\models\Development;
use common\models\DevelopmentPriority;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * DeveloppementController implements the CRUD actions for Developpement model.
 */
class DevelopmentController extends Controller 
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::hasAccessBackend() ;
                        }
                    ]
                ],
            ],
        ];
    }

    /**
     * Liste les développements.
     * 
     * @return mixed
     */
    public function actionIndex($status = Development::STATUS_OPEN) 
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Development::find()
                ->with(['developmentPriority', 'developmentPriorityCurrentProducer'])
                ->where(['status' => $status])
                ->orderBy('date DESC'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'status' => $status
        ]); 
    }

    /**
     * Creates a new Developpement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() 
    {
        $model = new Development();

        if ($model->load(Yii::$app->request->post())) {
            $model->date = date('Y-m-d H:i:s');
            $model->setDateDelivery();
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Développement ajouté');
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Developpement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) 
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->setDateDelivery();
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Développement modifié');
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Developpement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) 
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', 'Développement supprimé');

        return $this->redirect(['index']);
    }

    /**
     * Définit une priorité pour un développement.
     * 
     * @param integer $idDevelopment
     * @param string $priorite
     */
    public function actionPriority($idDevelopment, $priority = null) 
    {
        $develpmentPriority = DevelopmentPriority::searchOne([
            'id_development' => $idDevelopment,
        ]) ;

        if (in_array($priority, [DevelopmentPriority::PRIORITY_HIGH,
                    DevelopmentPriority::PRIORITY_NORMAL,
                    DevelopmentPriority::PRIORITY_LOW])) {

            if ($develpmentPriority) {
                $develpmentPriority->priority = $priority;
                $develpmentPriority->id_producer = GlobalParam::getCurrentProducerId();
            } else {
                $develpmentPriority = new DevelopmentPriority;
                $develpmentPriority->id_development = $idDevelopment;
                $develpmentPriority->priority = $priority;
                $develpmentPriority->id_producer = GlobalParam::getCurrentProducerId();
            }

            $develpmentPriority->save();
        } else {
            if ($develpmentPriority) {
                $develpmentPriority->delete();
            }
        }

        $this->redirect(['index']);
    }

    /**
     * Finds the Developpement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Developpement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) 
    {
        if (($model = Development::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
