<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use common\models\Producer ;
use common\models\User ;

/**
 * UserController implements the CRUD actions for User model.
 */
class CommunicateController extends BackendController 
{

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::hasAccessBackend();
                        }
                    ]
                ],
            ],
        ];
    }

    /**
     * Affiche la page d'accueil de la section avec un aperçu du mpde d'emploi
     * à imprimer.
     * 
     * @return string
     */
    public function actionIndex() 
    {
        $producer = Producer::searchOne() ;
        $pointsSaleArray = PointSale::searchAll() ; 
        
        return $this->render('index', [
            'producer' => $producer,
            'pointsSaleArray' => $pointsSaleArray,
        ]);
    }

    /**
     * Génére un PDF contenant le mode d'emploi d'utilisation de la plateforme
     * à destination des clients des producteurs.
     * 
     * @return string
     */
    public function actionInstructions() 
    {
        $producer = Producer::searchOne() ;

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('instructions_multi', [
            'pdf' => true,
            'producer' => $producer
        ]);

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            'marginRight' => 0,
            'marginLeft' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssFile' => '@app/web/css/screen.css',
                // any css to be embedded if required
                //'cssInline' => '.kv-heading-1{font-size:18px}', 
                // set mPDF properties on the fly
                //'options' => ['title' => 'Krajee Report Title'],
                // call mPDF methods on the fly

                /* 'methods' => [ 
                  'SetHeader'=>['Commandes du '.$date_str],
                  'SetFooter'=>['{PAGENO}'],
                  ] */
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

}
