<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace backend\controllers;

use common\helpers\GlobalParam;
use Yii;
use yii\base\UserException;


class QuotationController extends DocumentController
{
        public function behaviors()
        {
                return [
                        'verbs' => [
                                'class' => VerbFilter::className(),
                                'actions' => [
                                ],
                        ],
                        'access' => [
                                'class' => AccessControl::className(),
                                'rules' => [
                                        [
                                                'allow' => true,
                                                'roles' => ['@'],
                                                'matchCallback' => function ($rule, $action) {
                                                        return User::hasAccessBackend();
                                                }
                                        ]
                                ],
                        ],
                ];
        }

        /**
         * Liste les modèles Invoice.
         *
         * @return mixed
         */
        public function actionIndex()
        {
                $searchModel = new QuotationSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                ]);
        }

        public function actionTransform($id)
        {
                $quotation = $this->findModel($id) ;
                if($quotation->isStatusValid()) {

                        $invoice = new Invoice ;
                        $invoice->id_producer = GlobalParam::getCurrentProducerId();
                        $invoice->id_user = $quotation->id_user ;
                        $invoice->address = $quotation->address ;
                        $invoice->comment = $quotation->comment ;
                        $invoice->name = str_replace(['Devis', 'devis'], 'Facture', $quotation->name);
                        $invoice->save() ;

                        Order::updateAll([
                                'order.id_invoice' => $invoice->id
                        ], [
                                'order.id_quotation' => $id
                        ]) ;

                        Yii::$app->getSession()->setFlash('success', 'Le devis <strong>'.Html::encode($quotation->name).'</strong> a bien été transformé en facture.');
                        return $this->redirect(['/' . $this->getControllerUrl() . '/index']);
                }
                else {
                        throw new UserException('Vous ne pouvez pas transformer en facture un devis non validé.');
                }
        }

}
