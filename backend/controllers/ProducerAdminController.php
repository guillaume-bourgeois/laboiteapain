<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\controllers;

use Yii;
use common\models\User;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\Upload;
use common\models\Producer;
use yii\data\ActiveDataProvider;
use common\models\Invoice;
use common\models\Product;
use common\models\Order;
use common\models\Subscription;

/**
 * UserController implements the CRUD actions for User model.
 */
class ProducerAdminController extends BackendController 
{

    public function behaviors() 
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::getCurrentStatus() == USER::STATUS_ADMIN;
                        }
                    ]
                ],
            ],
        ];
    }

    /**
     * Liste les producteurs.
     * 
     * @return mixed
     */
    public function actionIndex() 
    {
        $dataProviderProducer = new ActiveDataProvider([
            'query' => Producer::find()
                    ->with('userProducer', 'user')
                    ->orderBy('active DESC, free_price DESC'),
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);

        $producersArray = Producer::find()->where('active = 1')->all();

        $sumFreePrice = 0 ;
        foreach($producersArray as $producer) {
                $sumFreePrice += $producer->free_price ;
        }

        return $this->render('index', [
            'dataProviderProducer' => $dataProviderProducer,
            'sumFreePrice' => $sumFreePrice
        ]);
    }
    
    /**
     * Crée un producteur.
     * 
     * @return mixed
     */
    public function actionCreate() 
    {
        $model = new Producer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Producteur créé.');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Génère la facture mensuelle d'un producteur.
     * 
     * @param integer $idProducer
     */
    public function actionBill($idProducer) 
    {
        $producer = Producer::findOne($idProducer);

        if ($producer) {
            $period = date('Y-m', strtotime('-1 month'));

            $last_invoice = Invoice::getLastInvoice() ;
            if (!$last_invoice) {
                $reference = 'BAP000001';
            } else {
                $reference = str_replace('BAP', '', $last_invoice->reference);
                $reference ++;
                $reference = 'BAP' . $reference;
            }

            $invoice = new Invoice;
            $invoice->id_producer = $idProducer;
            $invoice->date = date('Y-m-d H:i:s');
            $invoice->reference = $reference;
            $invoice->turnover = $producer->getTurnover($period);
            $invoice->amount_ht = $producer->getFreePrice() ;
            $invoice->wording = 'Facture ' . date('m/Y', strtotime('-1 month'));
            $invoice->text = 'Utilisation de la plateforme <strong>distrib</strong> pour le mois : ' . date('m/Y', strtotime('-1 month')) . '<br />'
                    . 'Chiffre d\'affaire réalisé sur la plateforme : <strong>' . number_format($facture->ca, 2) . ' €</strong> commissionné à <strong>1%</strong>.';
            $invoice->paid = 0;
            $invoice->period = $period;
            $invoice->save();
        }

        $this->redirect(['producer-admin/index']);
    }

    /**
     * Liste les factures des producteurs.
     * 
     * @return mxied
     */
    public function actionBilling() 
    {
        $dataProviderInvoice = new ActiveDataProvider([
            'query' => Invoice::find()
                    ->with('producer')
                    ->orderBy('reference DESC'),
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);

        return $this->render('billing', [
            'dataProviderInvoice' => $dataProviderInvoice,
        ]);
    }
    
    public function actionProducerInstallTaxUpdatePrices($idProducer)
    {
            // product
            $productsArray = Product::searchAll([
                'id_producer' => $idProducer
            ]) ;

            $connection = Yii::$app->getDb();
            
            foreach($productsArray as $product) {
                    $product->price = round($product->price / (1 + $product->taxRate->value), 2) ;
                    $product->save() ;

                    $command = $connection->createCommand("
                        UPDATE `product_order`
                        SET price = ROUND(price / (1 + :tax_value), 2),
                            id_tax_rate = :id_tax_rate
                        WHERE id_product = :id_product",
                            [
                                    ':id_product' => $product->id,
                                    ':tax_value' => $product->taxRate->value,
                                    ':id_tax_rate' => $product->taxRate->id,
                            ]);

                    $result = $command->query();
            }

            echo 'ok' ;
    }

    /**
     * Recherche un établissement.
     * 
     * @param integer $id
     * @return Etablissement
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($model = Producer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
