<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace backend\models;

use common\helpers\GlobalParam;
use Yii;
use yii\base\Model;
use common\models\CreditHistory ;
use common\models\User ;
use common\models\Producer ;
use common\models\UserProducer ;
use common\helpers\Mail ;

/**
 * ContactForm is the model behind the contact form.
 */
class CreditForm extends Model 
{

    public $id_user ;
    public $id_user_action ;
    public $id_producer ;
    public $type ;
    public $amount ;
    public $mean_payment ;
    public $comment ;
    public $send_mail ;

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['amount'], 'required'],
            [['id_user', 'id_user_action', 'id_producer'], 'integer'],
            [['date','send_mail'], 'safe'],
            [['amount'], 'double'],
            [['type', 'mean_payment', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id_user' => 'Utilisateur',
            'id_user_action' => 'Utilisateur',
            'date' => 'Date',
            'amount' => 'Montant',
            'type' => 'Type',
            'id_producer' => 'Producteur',
            'mean_payment' => 'Moyen de paiement',
            'comment' => 'Commentaire',
            'send_mail' => 'Prévenir l\'utilisateur',
        ];
    }
    
    /**
     * Enregistre un modèle CreditHistorique.
     */
    public function save() 
    {
        if ($this->validate()) {
            $creditHistory = new CreditHistory;
            $creditHistory->id_user = $this->id_user;
            $creditHistory->id_user_action = Yii::$app->user->identity->id;
            $creditHistory->id_producer = GlobalParam::getCurrentProducerId() ;
            $creditHistory->type = $this->type ;
            $creditHistory->comment = $this->comment ;
            $creditHistory->amount = $this->amount ;
            $creditHistory->mean_payment = $this->mean_payment ;
            $creditHistory->save();
            
            // on prévient l'utilisateur que son compte vient d'être crédité
            if($this->send_mail) {
                $user = User::findOne($this->id_user) ;
                $producer = GlobalParam::getCurrentProducer() ;
                $userProducer = UserProducer::searchOne([
                    'id_user' => $this->id_user
                ]);
                Mail::send($user->email, 'Votre compte vient d\'être crédité','creditUser', [   
                    'user' => $user, 
                    'producer' => $producer,
                    'userProducer' => $userProducer,
                    'creditForm' => $this
                ]) ;
            }
        }
    }

}
