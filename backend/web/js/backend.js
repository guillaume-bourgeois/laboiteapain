/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

$(document).ready(function() {
	    opendistrib_datepicker() ;
        $('button[data-toggle=popover]').popover() ;
        opendistrib_commandeauto() ;
        opendistrib_points_vente_acces() ;
        opendistrib_tooltip() ;
        opendistrib_ordre_produits() ;
        opendistrib_ordre_categories() ;
        opendistrib_products() ;
        opendistrib_product_prices() ;
        opendistrib_confirm_delete() ;
}) ;

var UrlManager = {
    getBaseUrl: function() {
        return $('meta[name=baseurl]').attr('content')+'/' ;
            
    },
    getBaseUrlAbsolute: function() {
        return $('meta[name=baseurl-absolute]').attr('content')+'/' ;
    }
};

function opendistrib_confirm_delete() {
    $('.btn-confirm-delete').click(function(event) {
        if(!confirm('Souhaitez-vous vraiment supprimer cette entrée ?')) {
            event.stopPropagation() ;
            return false ;   
        }
    }) ;
}

function opendistrib_products() {
    if($('.product-create').size() || $('.product-update').size()) {
        opendistrib_products_event_unit(false) ;

        $('#product-unit').change(function() {
            opendistrib_products_event_unit(true) ;
        }) ;


        opendistrib_products_event_price_with_tax() ;

        $('#product-price').change(opendistrib_products_event_price_with_tax);
        $('#product-price-with-tax').change(opendistrib_products_event_price);
        $('#product-id_tax_rate').change(opendistrib_products_event_price_with_tax);

    }
}

function opendistrib_products_event_price_with_tax() {
    taxRateSelected = $('#product-id_tax_rate').find('option:selected').data('tax-rate-value');
    if(typeof taxRateSelected == 'undefined') {
        taxRateSelected = 0 ;
    }

    var price = $('#product-price').val() ;
    if(price) {
        $('#product-price-with-tax').val(getPriceWithTax(price, taxRateSelected));
        // formattage
        $('#product-price').val(parseFloat(price).toFixed(3));
    }
}
function opendistrib_products_event_price(){
    taxRateSelected = $('#product-id_tax_rate').find('option:selected').data('tax-rate-value');
    var priceWithTax = $('#product-price-with-tax').val() ;
    if(priceWithTax) {
        $('#product-price').val(getPrice(priceWithTax, taxRateSelected));
        // formattage
        $('#product-price-with-tax').val(parseFloat(priceWithTax).toFixed(2));
    }
}


function opendistrib_products_event_unit(change) {
    var unit = $('#product-unit').val() ;
    if(unit == 'piece') {
        $('.field-product-step').hide() ;
        $('.field-product-weight label').html('Poids (g)') ;
        $('.field-product-weight').show() ;
    }
    else {
        $('.field-product-step').show() ;
        $('.field-product-weight label').html('Poids ('+$('#product-unit').val()+')') ;
        $('.field-product-weight').hide() ;
    }
    
    var label_price_ttc = $('.field-product-price .control-label.with-tax') ;
    var label_price_ht = $('.field-product-price .control-label.without-tax') ;
    var label_step = $('.field-product-step .control-label') ;
    var label_quantity_max = $('.field-product-quantity_max .control-label') ; 
    
    if(unit == 'piece') {
        label_price_ttc.html('Prix (la pièce) TTC') ;
        label_price_ht.html('Prix (la pièce) HT') ;
        label_quantity_max.html('Quantité max par défaut (pièces)') ;
    }
    else if(unit == 'g' || unit == 'kg') {
        label_price_ttc.html('Prix (au kg) TTC') ;
        label_price_ht.html('Prix (au kg) HT') ;
        label_quantity_max.html('Quantité max par défaut (kg)') ;
        label_step.html('Pas ('+unit+')') ;
    }
    else if(unit == 'mL' || unit == 'L') {
        label_price_ttc.html('Prix (au litre) TTC') ;
        label_price_ht.html('Prix (au litre) HT') ;
        label_quantity_max.html('Quantité max par défaut (litres)') ;
        label_step.html('Pas ('+unit+')') ;
    }
    
    if(change) {
        if(unit == 'piece') {
            $('#product-step').val(1) ;
        }
        else {
            $('#product-step').val('') ;
        }
    }
    
}

function opendistrib_product_prices() {
    if($('.product-prices-create').size() || $('.product-prices-update').size()) {
        opendistrib_product_prices_event_price_with_tax() ;
        $('#productprice-price').change(opendistrib_product_prices_event_price_with_tax);
        $('#productprice-price-with-tax').change(opendistrib_product_prices_event_price);
    }
}

function opendistrib_product_prices_event_price_with_tax() {
    var taxRateValue = $('#productprice-price-with-tax').data('tax-rate-value');
    var price = $('#productprice-price').val() ;
    if(price) {
        $('#productprice-price-with-tax').val(getPriceWithTax(price, taxRateValue));
        // formattage
        $('#productprice-price').val(parseFloat(price).toFixed(3));
    }
}

function opendistrib_product_prices_event_price() {
    var taxRateValue = $('#productprice-price-with-tax').data('tax-rate-value');
    var priceWithTax = $('#productprice-price-with-tax').val() ;
    if(priceWithTax) {
        $('#productprice-price').val(getPrice(priceWithTax, taxRateValue));
        // formattage
        $('#productprice-price-with-tax').val(parseFloat(priceWithTax).toFixed(2));
    }
}

function opendistrib_tooltip() {
    $('[data-toggle="tooltip"]').tooltip({container:'body'});
}

function opendistrib_nl2br(str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function opendistrib_points_vente_acces() {
    // affichage du bloc acces restreint
    $('#pointsale-restricted_access').change(function() {
        opendistrib_points_vente_acces_event() ;
    }) ;
    opendistrib_points_vente_acces_event() ;
    
    // affichage du champs commentaire
    $('#pointsale-users input[type=checkbox]').change(function() {
        opendistrib_points_vente_commentaire_event() ;
    }) ;
    opendistrib_points_vente_commentaire_event() ;
}

function opendistrib_points_vente_commentaire_event() {
    $('#pointsale-users input[type=checkbox]').each(function() {
        if($(this).prop('checked')) {
            $(this).parent().find('.commentaire').fadeIn() ;
        }
        else {
            $(this).parent().find('.commentaire').hide() ;
        }
    }) ;
}

function opendistrib_points_vente_acces_event() {
    if($('#pointsale-restricted_access').prop('checked')) {
        $('#pointsale-users').fadeIn() ;
    }
    else {
        $('#pointsale-users').hide() ;
    }
}

function opendistrib_commandeauto() {
    $('#subscriptionform-date_begin, #subscriptionform-date_end').datepicker() ;
}

function opendistrib_sortable_list(element_selector, button_selector, route_ajax) {
    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    $(element_selector+" table tbody").sortable({
        items: "> tr",
        appendTo: "parent",
        cursor: "move",
        placeholder: "ui-state-highlight",
        handle: button_selector,
        helper: fixHelper,
        stop: function(event, ui) {
            var tab_ordre = {} ;
            var ordre = 1 ;

            if($('ul.pagination').size()) {
                var page = parseInt($('ul.pagination li.active a').html()) ;
                var nb_items_by_page = parseInt($('#page-size').html()) ;
                if(page != 1) {
                    ordre = (page - 1) * nb_items_by_page ;
                }
            }

            $(element_selector+" table tbody tr").each(function() {
                tab_ordre[$(this).attr('data-key')] = ordre ;
                ordre++ ;
            }) ;

            $.post(UrlManager.getBaseUrl() + route_ajax,{
                array: JSON.stringify(tab_ordre)
            }) ;
        }
    }).disableSelection();
}

function opendistrib_ordre_categories() {
    opendistrib_sortable_list(
        '.product-category-index',
        '.btn-position',
        'product-category/position'
    ) ;
}

function opendistrib_ordre_produits() {

    opendistrib_sortable_list(
        '.product-index',
        '.btn-order',
        'product/order'
    ) ;

    /*var fixHelper = function(e, ui) {
        ui.children().each(function() {  
          $(this).width($(this).width());  
        });  
        return ui;  
      };
    
    $(".product-index table tbody").sortable({
        items: "> tr",
        appendTo: "parent",
        cursor: "move",
        placeholder: "ui-state-highlight",
        handle: '.btn-order',
        //helper: "clone"
        helper: fixHelper,
        stop: function(event, ui) {
            var tab_ordre = {} ;
            var ordre = 1 ;
            
            if($('ul.pagination').size()) {
                var page = parseInt($('ul.pagination li.active a').html()) ;
                var nb_items_by_page = parseInt($('#page-size').html()) ;
                if(page != 1) {
                    ordre = (page - 1) * nb_items_by_page ;
                }
            }
            
            $(".product-index table tbody tr").each(function() {
                tab_ordre[$(this).attr('data-key')] = ordre ;
                ordre++ ;
            }) ;
            
            $.post(UrlManager.getBaseUrl()+'product/order',{
                array: JSON.stringify(tab_ordre)
            }) ;
        }
    }).disableSelection();*/
}

function opendistrib_datepicker() {
    $('input.datepicker').datepicker({dateFormat:'dd/mm/yy'}) ;
}

/* French initialisation for the jQuery UI date picker plugin. */
/* Written by Keith Wood (kbwood{at}iinet.com.au),
			  StÃ©phane Nahmani (sholby@sholby.net),
			  StÃ©phane Raimbault <stephane.raimbault@gmail.com> */
(function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define([ "../jquery.ui.datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}(function( datepicker ) {
	datepicker.regional['fr'] = {
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
			'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
		monthNamesShort: ['janv.', 'févr.', 'mars', 'avril', 'mai', 'juin',
			'juil.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'],
		dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
		dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
		dayNamesMin: ['D','L','M','M','J','V','S'],
		weekHeader: 'Sem.',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	datepicker.setDefaults(datepicker.regional['fr']);

	return datepicker.regional['fr'];

}));

