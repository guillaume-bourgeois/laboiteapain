
var appAlerts = new Vue({
    el: '#app-alerts',
    data: {
        alerts: [
        ]
    },
    mounted: function() {

    },
    methods: {
        alertResponse: function(response) {
            if(response.data.alert) {
                this.alert(response.data.alert.type, response.data.alert.message) ;
            }
        },
        alert: function(type, message, timeout) {
            var app = this ;
            var index = this.alerts.length ;
            this.alerts.splice(index, 0, {
                display: false,
                type: type,
                message: message
            });

            var functionDisplayAlert = function(app, index, display) {
                app.alerts[index].display = display ;
            } ;

            if(!timeout) {
                timeout = 3000 ;
            }

            setTimeout(functionDisplayAlert, 500, app, index, true) ;
            setTimeout(functionDisplayAlert, timeout, app, index, false) ;

        }
    }
});