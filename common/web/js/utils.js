/**
 * Created by fab on 30/12/17.
 */

/**
 * Retourne un prix sans taxe sur base du prix avec tax
 *
 * @param priceWithTax
 * @param taxRate
 * @returns {string}
 */
function getPrice(priceWithTax, taxRate) {
    return numberDecimals(parseFloat(parseFloat(priceWithTax) / (taxRate + 1)), 3);
}

/**
 * Retourne un prix avec taxe sur base du prix sans taxe
 *
 * @param priceWithoutTax
 * @param taxRate
 * @returns {string}
 */
function getPriceWithTax(priceWithoutTax, taxRate) {
    return numberDecimals(parseFloat(parseFloat(priceWithoutTax) * (taxRate + 1)), 2);
}

function numberDecimals(num, decimals) {
    return Number(num).toFixed(decimals) ;
}

/**
 * Formate un prix en l'arrondissant et en ajoutant le sigle de la monnaie
 *
 * @param price
 * @returns {string}
 */
function formatPrice(price) {
    return numberDecimals(price, 2).replace('.', ',') + ' €';
}

/**
 * Formate une date au format jj/mm/yyyy
 * @param date
 * @returns {*}
 */
function formatDate(date) {
    if (date) {
        return ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
    }
    return false;
}

/**
 * Equivalent de console.log (ne déclenche pas d'erreur si la console est fermé)
 *
 * @param msg
 */

function log(msg) {
    try {
        console.log(msg);
    } catch (e) {

    }
}

/**
 * Convertit un formulaire ou un objet en JSON (utilisé pour l'envoie de donnée en ajax)
 */
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


function getDateFormatted(date) {
    var _d = date.getDate(),
        d = _d > 9 ? _d : '0' + _d,
        _m = date.getMonth() + 1,
        m = _m > 9 ? _m : '0' + _m,
        formatted = date.getFullYear() + '-' + m + '-' + d;
    return formatted;
}

//Affiche une alert au click sur un bouton submit lorsqu'un utilisateur admin tente de modifer un Ã©tablissement
function userNotAllowToEdit() {
    alert('Vous n\'êtes pas autorisé à effectuer cette action');
    return false;
}
