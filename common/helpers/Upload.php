<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\helpers;

use yii\web\UploadedFile;
use Yii ;

class Upload 
{

    public static function uploadFile($model, $champs, $filename_old = '') 
    {
        $file = UploadedFile::getInstance($model, $champs);
        if ($file) {
            $file_name = $file->baseName . '-' . uniqid() ;
            $file_name_extension = $file_name .'.'.$file->extension ;
            $dir_file = '../../producer/web/uploads/' ;
            $path_file = $dir_file . $file_name_extension ;
            $file->saveAs($path_file);
            
            // resize image
            list($width, $height, $type, $attr) = getimagesize($path_file);
            if($width > 500)
            {
                $image = Yii::$app->image->load($path_file);
                
                // big
                if($width > 1600)
                {
                    $image->resize(1600)
                          ->save($dir_file . $file_name . '-big.'.$file->extension);
                }
                
                // medium
                if($width > 1024)
                {
                    $image->resize(1024)->save($dir_file . $file_name_extension) ;
                    $image->save($dir_file . $file_name . '-medium.'.$file->extension);
                       
                       
                }
                
                // small
                if($width > 500)
                {
                    $image->resize(500)
                       ->save($dir_file . $file_name . '-small.'.$file->extension);
                }
            }
            
            $model->$champs = $file_name_extension ;
        } else {
            $model->$champs = $filename_old;
        }

        $model->save();
    }

}
