<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\helpers;

class Departments {

    public static function get() 
    {
        $depts = array();
        $depts["01"] = "Ain";
        $depts["02"] = "Aisne";
        $depts["03"] = "Allier";
        $depts["04"] = "Alpes de Haute Provence";
        $depts["05"] = "Hautes Alpes";
        $depts["06"] = "Alpes Maritimes";
        $depts["07"] = "Ardèche";
        $depts["08"] = "Ardennes";
        $depts["09"] = "Ariège";
        $depts["10"] = "Aube";
        $depts["11"] = "Aude";
        $depts["12"] = "Aveyron";
        $depts["13"] = "Bouches du Rhône";
        $depts["14"] = "Calvados";
        $depts["15"] = "Cantal";
        $depts["16"] = "Charente";
        $depts["17"] = "Charente Maritime";
        $depts["18"] = "Cher";
        $depts["19"] = "Corrèze";
        $depts["2A"] = "Corse du Sud";
        $depts["2B"] = "Haute Corse";
        $depts["21"] = "Côte d'Or";
        $depts["22"] = "Côtes d'Armor";
        $depts["23"] = "Creuse";
        $depts["24"] = "Dordogne";
        $depts["25"] = "Doubs";
        $depts["26"] = "Drôme";
        $depts["27"] = "Eure";
        $depts["28"] = "Eure et Loir";
        $depts["29"] = "Finistère";
        $depts["30"] = "Gard";
        $depts["31"] = "Haute Garonne";
        $depts["32"] = "Gers";
        $depts["33"] = "Gironde";
        $depts["34"] = "Hérault";
        $depts["35"] = "Ille et Vilaine";
        $depts["36"] = "Indre";
        $depts["37"] = "Indre et Loire";
        $depts["38"] = "Isère";
        $depts["39"] = "Jura";
        $depts["40"] = "Landes";
        $depts["41"] = "Loir et Cher";
        $depts["42"] = "Loire";
        $depts["43"] = "Haute Loire";
        $depts["44"] = "Loire Atlantique";
        $depts["45"] = "Loiret";
        $depts["46"] = "Lot";
        $depts["47"] = "Lot et Garonne";
        $depts["48"] = "Lozère";
        $depts["49"] = "Maine et Loire";
        $depts["50"] = "Manche";
        $depts["51"] = "Marne";
        $depts["52"] = "Haute Marne";
        $depts["53"] = "Mayenne";
        $depts["54"] = "Meurthe et Moselle";
        $depts["55"] = "Meuse";
        $depts["56"] = "Morbihan";
        $depts["57"] = "Moselle";
        $depts["58"] = "Nièvre";
        $depts["59"] = "Nord";
        $depts["60"] = "Oise";
        $depts["61"] = "Orne";
        $depts["62"] = "Pas de Calais";
        $depts["63"] = "Puy de Dôme";
        $depts["64"] = "Pyrénées Atlantiques";
        $depts["65"] = "Hautes Pyrénées";
        $depts["66"] = "Pyrénées Orientales";
        $depts["67"] = "Bas Rhin";
        $depts["68"] = "Haut Rhin";
        $depts["69"] = "Rhône";
        $depts["70"] = "Haute Saône";
        $depts["71"] = "Saône et Loire";
        $depts["72"] = "Sarthe";
        $depts["73"] = "Savoie";
        $depts["74"] = "Haute Savoie";
        $depts["75"] = "Paris";
        $depts["76"] = "Seine Maritime";
        $depts["77"] = "Seine et Marne";
        $depts["78"] = "Yvelines";
        $depts["79"] = "Deux Sèvres";
        $depts["80"] = "Somme";
        $depts["81"] = "Tarn";
        $depts["82"] = "Tarn et Garonne";
        $depts["83"] = "Var";
        $depts["84"] = "Vaucluse";
        $depts["85"] = "Vendée";
        $depts["86"] = "Vienne";
        $depts["87"] = "Haute Vienne";
        $depts["88"] = "Vosges";
        $depts["89"] = "Yonne";
        $depts["90"] = "Territoire de Belfort";
        $depts["91"] = "Essonne";
        $depts["92"] = "Hauts de Seine";
        $depts["93"] = "Seine St Denis";
        $depts["94"] = "Val de Marne";
        $depts["95"] = "Val d'Oise";
        $depts["97"] = "DOM";
        
        return $depts ;
    }
    


}