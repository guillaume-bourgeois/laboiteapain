<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models;

use Yii ;
use yii\base\Object;
use common\components\ActiveRecordCommon ;

/**
 * This is the model class for table "production_produit".
 *
 * @property integer $id
 * @property integer $id_distribution
 * @property integer $id_product
 * @property integer $active
 */
class ProductDistribution extends ActiveRecordCommon 
{

    /**
     * @inheritdoc
     */
    public static function tableName() 
    {
        return 'product_distribution';
    }

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['id_distribution', 'id_product', 'active'], 'integer'],
            [['quantity_max'], 'number'],
        ];
    }

    /*
     * Relations
     */
    
    public function getProduct() 
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product']);
    }
    
    public function getDistribution() 
    {
        return $this->hasOne(Distribution::className(), ['id' => 'id_distribution']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id' => 'ID',
            'id_distribution' => 'Distribution',
            'id_product' => 'Produit',
            'active' => 'Actif',
            'quantity_max' => 'Quantité max',
        ];
    }
    
    /**
     * Retourne les options de base nécessaires à la fonction de recherche.
     * 
     * @return array
     */
    public static function defaultOptionsSearch() {
        return [
            'with' => ['product','distribution'],
            'join_with' => [],
            'orderby' => '',
            'attribute_id_producer' => ''
        ] ;
    }

    /**
     * Recherche les enregistrement ProductionProduit liés à une production.
     * 
     * @param integer $idDistribution
     * @return array
     */
    public static function searchByDistribution($idDistribution) 
    {
        $arrayProductsDistribution = ProductDistribution::searchAll([
            'id_distribution' => $idDistribution
        ]) ;
       
        $orders = Order::searchAll([
            'distribution.id' => $idDistribution
        ]) ;

        foreach ($arrayProductsDistribution as $productDistribution) {
            if (isset($productDistribution->product)) {
                $arrayProductsDistribution[$productDistribution->id_product] = [
                    'active' => (int) $productDistribution->active,
                    'unavailable' => (int) $productDistribution->product->unavailable,
                    'quantity_max' => $productDistribution->quantity_max,
                    'quantity_order' => Order::getProductQuantity($productDistribution->id_product, $orders),
                    'quantity_remaining' => $productDistribution->quantity_max - Order::getProductQuantity($productDistribution->id_product, $orders)
                ];
            }
        }

        return $arrayProductsDistribution;
    }
    
}
