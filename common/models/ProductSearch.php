<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models ;

use common\helpers\GlobalParam;
use common\models\Product ;

class ProductSearch extends Product 
{
    public function rules() 
    {
        return [
            [['active', 'order', 'quantity_max', 'id_producer'], 'integer'],
            [['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'unavailable'], 'boolean'],
            [['price', 'weight'], 'number'],
            [[ 'photo'], 'file'],
            [['name', 'description', 'photo'], 'string', 'max' => 255],
            [['recipe'], 'string', 'max' => 1000],
        ];
    }
    
    public function search($params) 
    {
        $optionsSearch = self::defaultOptionsSearch() ;
        
        $query = Product::find()
            ->with($optionsSearch['with'])
            ->innerJoinWith($optionsSearch['join_with'], true)
            ->where(['product.id_producer' => GlobalParam::getCurrentProducerId()])
            ->orderBy('product.order ASC')
            ;
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['order', 'photo', 'name', 'description','active']],
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);
        
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'product.name', $this->name]) ;
        $query->andFilterWhere(['like', 'product.description', $this->description]) ;
        
        if(isset($this->active) && is_numeric($this->active)) {
            $query->andWhere([
                'product.active' => $this->active
            ]) ;    
        }
        
        return $dataProvider;
    }
}