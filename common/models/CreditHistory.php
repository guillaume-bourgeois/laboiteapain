<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace common\models;

use Yii;
use common\components\ActiveRecordCommon ;
use yii\db\ActiveRecord;
use common\models\User;
use common\models\Order;
use common\models\Producer;
use yii\helpers\Html;

/**
 * This is the model class for table "credit_historique".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_order
 * @property string $date
 * @property double $amount
 * @property string $type
 * @property integer $id_producer
 * @property string $mean_payment
 */
class CreditHistory extends ActiveRecordCommon
{

    const TYPE_INITIAL_CREDIT       = 'initial-credit';
    const TYPE_CREDIT               = 'credit';
    const TYPE_PAYMENT              = 'payment';
    const TYPE_REFUND               = 'refund';
    const TYPE_DEBIT                = 'debit';
    
    /**
     * @inheritdoc
     */
    public static function tableName() 
    {
        return 'credit_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['amount'], 'required'],
            [['id_user', 'id_user_action', 'id_order', 'id_producer'], 'integer'],
            [['date'], 'safe'],
            [['amount'], 'double'],
            [['type', 'mean_payment', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id' => 'ID',
            'id_user' => 'Utilisateur',
            'id_user_action' => 'Utilisateur',
            'id_order' => 'Commande',
            'date' => 'Date',
            'amount' => 'Montant',
            'type' => 'Type',
            'id_producer' => 'Producteur',
            'mean_payment' => 'Moyen de paiement',
            'comment' => 'Commentaire',
        ];
    }
    
    

    /*
     * Relations
     */
    
    public function getUser() 
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getUserAction() 
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_action']);
    }

    public function getOrder() 
    {
        return $this->hasOne(Order::className(), ['id' => 'id_order']);
    }
    
    /**
     * Retourne les options de base nécessaires à la fonction de recherche.
     * 
     * @return array
     */
    public static function defaultOptionsSearch() {
        return [
            'with' => [],
            'join_with' => [],
            'orderby' => self::tableName().'.date ASc',
            'attribute_id_producer' => self::tableName().'.id_producer'
        ] ;
    }

    /**
     * Retourne le type de CreditHistorique (paiement, remboursement ou débit).
     * 
     * @return string
     */
    public function getStrType() 
    {
        switch ($this->type) {
            case self::TYPE_PAYMENT:
                return 'Paiement';
                break;
            case self::TYPE_REFUND:
                return 'Remboursement';
                break;
            case self::TYPE_DEBIT:
                return 'Débit';
                break;
        }
    }

    /**
     * Enregistre le modèle.
     * 
     * @param boolean $runValidation
     * @param array $attributeNames
     */
    public function save($runValidation = true, $attributeNames = NULL) 
    {
        // initialisation du commentaire avant sauvegarde
        $this->comment .= $this->getStrComment() ;
        
        parent::save($runValidation, $attributeNames);

        // mise à jour du crédit au niveau de UserProducer
        $userProducer = UserProducer::searchOne([
            'id_user' => $this->id_user,
            'id_producer' => $this->id_producer
        ]) ;
        
        $creditLimitReminder = Producer::getConfig('credit_limit_reminder') ;
        $oldCredit = $userProducer->credit ;
        
        if ($userProducer) {
            if ($this->isTypeCredit()) {
                $userProducer->credit += $this->amount;
            } elseif ($this->isTypeDebit()) {
                $userProducer->credit -= $this->amount;
            }
            $userProducer->save();
            
            // set mean payment
            if($this->id_order && $this->id_order > 0) {
                $order = Order::searchOne(['id' => (int) $this->id_order]) ;
                if($order) {
                    $paymentStatus = $order->getPaymentStatus() ;
                    if($paymentStatus == Order::PAYMENT_PAID 
                        || $paymentStatus == Order::PAYMENT_SURPLUS) {
                        $order->mean_payment = MeanPayment::CREDIT ;
                        $order->save() ;
                    }
                }
            }
            
                    
            // seuil limite de crédit
            $newCredit = $userProducer->credit ;
            if(!is_null($creditLimitReminder) && 
               $oldCredit > $creditLimitReminder && $newCredit <= $creditLimitReminder) {
                $user = User::findOne($this->id_user) ;
                $producer = Producer::findOne($this->id_producer) ;
                Yii::$app->mailer->compose(
                        [
                         'html' => 'creditLimitReminder-html', 
                         'text' => 'creditLimitReminder-text'
                        ], 
                        [   
                            'user' => $user, 
                            'producer' => $producer, 
                            'credit' => $newCredit
                        ]
                    )
                    ->setTo($user->email)
                    ->setFrom(['contact@opendistrib.net' => 'distrib'])
                    ->setSubject('[distrib] Seuil limite de crédit dépassé')
                    ->send();
            }
        }        
    }
    
    /**
     * Retourne si le CreditHistorique est un débit ou non.
     * 
     * @return boolean
     */
    public function isTypeDebit() 
    {
        return in_array($this->type, [
            self::TYPE_DEBIT,
            self::TYPE_PAYMENT,
        ]) ;
    }
    
    /**
     * Retourne si le CreditHistorique est un crédit ou non.
     * 
     * @return boolean
     */
    public function isTypeCredit() 
    {
        return in_array($this->type, [
            self::TYPE_CREDIT,
            self::TYPE_INITIAL_CREDIT,
            self::TYPE_REFUND
        ]) ;
    }
    
    /**
     * Retourne le montant.
     * 
     * @param boolean $format
     * @return float
     */
    public function getAmount($format = false) 
    {
        if($format) {
            return number_format($this->amount,2) .'&nbsp;€' ;
        }
        else {
            return $this->amount ;
        }
    }
    
    /**
     * Retourne le libellé du CreditHistory informant de son type et 
     * éventuellement de la date de sa commande associée.
     * 
     * @return string
     */
    public function getStrWording() 
    {
        $str = '' ;
        
        if($this->type == self::TYPE_INITIAL_CREDIT) {
            $str = 'Crédit initial' ;
        }
        elseif($this->type == self::TYPE_CREDIT) {
            $str = 'Crédit' ;
        }
        elseif($this->type == self::TYPE_PAYMENT) {
            $str = 'Paiement' ;
        }
        elseif($this->type == self::TYPE_REFUND) {
            $str = 'Remboursement' ;
        }
        elseif($this->type == self::TYPE_DEBIT) {
            $str = 'Débit' ;
        }
        
        if($this->type == self::TYPE_PAYMENT || $this->type == self::TYPE_REFUND) {
            if(isset($this->order)) {
                $str .= '<br />Commande : '.date('d/m/Y',strtotime($this->order->distribution->date)) ;
            }
            else {
                $str .= '<br />Commande supprimée' ;
            }
        }
        
        return $str ;
    }
    
    
    /**
     * Retourne les informations à ajouter au commentaire du CreditHistorique 
     * (libellé, montant, client, action) au format HTML.
     * 
     * @return string
     */
    public function getStrComment() 
    {
        $str = '' ;
        if(strlen($this->comment)) {
            $str .= '<br />' ;
        }
        $str .= $this->getStrWording() ;
        if(isset($this->order)) {
            $str .= '<br />Montant de la commande : '.$this->order->getAmountWithTax(Order::AMOUNT_TOTAL, true) ;
        }
        if(isset($this->user)) {
            $str .= '<br />Client : '.Html::encode($this->user->name. ' '.$this->user->lastname) ;
        }
        if(isset($this->userAction)) {
            $str .= '<br />Action : '.Html::encode($this->userAction->name. ' '.$this->userAction->lastname) ;
        }
        
        return $str ;
    }
    
    /**
     * Retourne la date.
     * 
     * @param boolean $format
     * @return string
     */
    public function getDate($format = false) 
    {
        if($format)
            return date('d/m/Y à H:i:s',strtotime($this->date)) ;
        else
            return $this->date ;
    }
    
    /**
     * Retourne le libellé du moyen de paiement du CreditHistory courant.
     * 
     * @return string
     */
    public function getStrMeanPayment() 
    {
        return MeanPayment::getStrBy($this->mean_payment) ;
    }
    
    /**
     * Retourne le libellé de l'utilisateur ayant initié l'action.
     * 
     * @return string
     */
    public function strUserAction() 
    {
        if($this->userAction) {
            return $this->userAction->name . ' ' . $this->userAction->lastname ;
        }
        else {
            return 'Système' ;
        }
    }

}
