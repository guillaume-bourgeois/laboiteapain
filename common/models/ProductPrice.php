<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace common\models;

use common\helpers\GlobalParam;
use Yii;
use yii\helpers\Html;
use common\models\UserPointSale;
use common\models\PointSaleDistribution;
use common\components\ActiveRecordCommon;

/**
 * This is the model class for table "product_price".
 *
 * @property integer $id
 */
class ProductPrice extends ActiveRecordCommon
{

        /**
         * @inheritdoc
         */
        public static function tableName()
        {
                return 'product_price';
        }

        /**
         * @inheritdoc
         */
        public function rules()
        {
                return [
                        ['id_user', 'required', 'when' => function($model) {
                                return !$model->id_point_sale && !$model->id_user_group ;
                        }, 'message' => 'Vous devez renseigner au moins un utilisateur, un point de vente ou un groupe d\'utilisateur'],
                        ['id_point_sale', 'required', 'when' => function($model) {
                                return !$model->id_user && !$model->id_user_group ;
                        }, 'message' => 'Vous devez renseigner au moins un utilisateur, un point de vente ou un groupe d\'utilisateur'],
                        ['id_user_group', 'required', 'when' => function($model) {
                                return !$model->id_user && !$model->id_point_sale ;
                        }, 'message' => 'Vous devez renseigner au moins un utilisateur, un point de vente ou un groupe d\'utilisateur'],
                        [['id_product', 'price'], 'required'],
                        [['id_product', 'id_user', 'id_point_sale', 'id_user_group', 'percent'], 'integer'],
                        [['price'], 'double'],
                ];
        }

        /**
         * @inheritdoc
         */
        public function attributeLabels()
        {
                return [
                        'id' => 'ID',
                        'id_product' => 'Produit',
                        'id_user' => 'Utilisateur',
                        'id_point_sale' => 'Point de vente',
                        'id_user_group' => "Groupe d'utilisateur",
                        'price' => 'Prix (HT)',
                        'percent' => 'Pourcentage',
                ];
        }

        /*
         * Relations
         */

        public function getProduct()
        {
                return $this->hasOne(
                        Product::className(),
                        ['id' => 'id_product']
                );
        }

        public function getPointSale()
        {
                return $this->hasOne(
                        PointSale::className(),
                        ['id' => 'id_point_sale']
                );
        }

        public function getUserGroup()
        {
                return $this->hasOne(
                        UserGroup::className(),
                        ['id' => 'id_user_group']
                );
        }

        public function getUser()
        {
                return $this->hasOne(
                        User::className(),
                        ['id' => 'id_user']
                ) ;
        }

        /**
         * Retourne les options de base nécessaires à la fonction de recherche.
         *
         * @return array
         */
        public static function defaultOptionsSearch()
        {
                return [
                        'with' => ['user', 'pointSale'],
                        'join_with' => ['product'],
                        'orderby' => '',
                        'attribute_id_producer' => 'product.id_producer'
                ];
        }

        public static function percentValues()
        {
                $percentValues = [
                        '' => 'Aucun'
                ] ;

                for($i = -50 ; $i < 51 ; $i = $i + 5) {
                        $percentValues[$i] = $i.' %' ;
                }

                return $percentValues ;
        }

}
