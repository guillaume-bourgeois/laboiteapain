<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

use dosamigos\leaflet\types\LatLng;
use dosamigos\leaflet\layers\Marker;
use dosamigos\leaflet\layers\TileLayer;
use dosamigos\leaflet\LeafLet;
use dosamigos\leaflet\widgets\Map;

use common\helpers\Url ;

$this->setTitle('Logiciel libre de distribution alimentaire en circuit court') ;
$this->setMeta('description', 'Simplifiez la distribution de vos produits en circuit court grâce à des outils web adaptés.') ;

?>

<div id="presentation">
    <div id="presentation-distrib">
        <h1><span>Logiciel libre<br />
                de distribution alimentaire<br />
            en circuit court</span></h1>
        <p>simple, ouvert et participatif</p>
    </div>
    <div id="row-users-producers">
        <div class="col-md-6 producer">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">
                        <img class="img" src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/cagette.png" id="img-producer" />
                        <span class="the-title">Producteurs</span>
                    </h2>
                </div>
                <div class="panel-body">
                    <p class="presentation">Simplifiez la distribution de vos produits<br /> avec des outils adaptés.</p>
                    <a class="btn btn-primary" href="<?= Yii::$app->urlManagerFrontend->createUrl(['site/signup']) ; ?>"><span class="glyphicon glyphicon-user"></span> Je crée mon espace</a>
                    <!--<?php if($producerDemoAccount) : ?><a class="btn btn-default" href="<?= Yii::$app->urlManagerFrontend->createUrl(['site/producer', 'id' => $producerDemoAccount->id]) ; ?>"><span class="glyphicon glyphicon-blackboard"></span> Démonstration</a><?php endif; ?>-->
                    <a class="btn btn-default" href="<?= Yii::$app->urlManagerFrontend->createUrl(['site/contact']) ; ?>"><span class="glyphicon glyphicon-info-sign"></span> Demande d'informations</a>
                </div>
            </div>
        </div>
        <div class="col-md-6 users">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title">
                        <img class="img" src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/users.png" id="img-users" />
                        <span class="the-title">Clients</span>
                    </h2>
                </div>
                <div class="panel-body">
                    <p class="presentation">Réservez vos produits en ligne et récupérez votre commande
                    chez votre producteur ou dans un dépôt près de chez vous.</p>    
                    <a class="btn btn-primary" href="<?= Yii::$app->urlManagerFrontend->createUrl(['site/producers']) ; ?>"><span class="glyphicon glyphicon-search"></span> Je recherche un producteur</a>
                </div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <?php if(Yii::$app->user->isGuest && YII_ENV == 'demo'): ?>
    <div class="row" id="row-signup">
        <a class="btn btn-primary btn-lg" href="<?= Url::env('prod','frontend'); ?>"><span class="glyphicon glyphicon-circle-arrow-left"></span> Retour sur le site</a>
    </div> 
    <?php endif; ?>
    
    <div id="row-functionalities-rates">
        <div class="panel panel-default" id="block-functionnalities">
            <div class="panel-heading">
                <h2 class="panel-title">
                    <span class="glyphicon glyphicon-cog"></span> <span class="the-title">Fonctionnalités</span>
                </h2>
            </div>
            <div class="panel-body">
                <div class="block block-order col-md-6">
                    <img src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/order.png" />
                    <p>Prise de commande en ligne automatisée avec génération d'un récapitulatif par jour de distribution.</p>
                    <div class="clr"></div>
                </div>
                <div class="block block-payment col-md-6">
                    <img src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/payment.png" />
                    <p>Système de crédit permettant la comptabilisation des paiements.</p>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>
                <div class="block block-points-sale-products col-md-6">
                    <img src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/map-marker.png" />
                    <p>Gestion des différents points de vente et produits.</p>
                    <div class="clr"></div>
                </div>
                <div class="block block-subscriptions col-md-6">
                    <img src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/subscription.png" />
                    <p>Gestion des abonnement.</p>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>
                <div class="block block-communication col-md-6">
                    <img src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/megaphone.png" />
                    <p>Communication simplifiée avec les clients.</p>
                    <div class="clr"></div>
                </div>
                <div class="block block-evolution col-md-6">
                    <img src="<?= Yii::$app->urlManager->getBaseUrl(); ?>/img/idea.png" />
                    <p><a href="<?= Yii::$app->urlManager->createUrl(['site/contact']) ?>">Proposez-nous</a> vos idées afin de faire évoluer l'outil !</p>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
        <div class="panel panel-default" id="block-rates">
            <div class="panel-heading">
                <h2 class="panel-title">
                    <span class="glyphicon glyphicon-euro"></span> <span class="the-title">Tarifs</span>
                </h2>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2">Producteurs</th>
                            <th>Clients</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Abonnement mensuel</td>
                            <td><strong>Prix libre</strong></td>
                            <td><strong>Gratuit</strong></td>
                        </tr>
                        <tr>
                            <td>Formation & aide à la mise en place</td>
                            <td><strong>30 € HT / heure</strong></td>
                            <td>Possibilité de don<br />lors de la commande</td>
                        </tr>
                        <tr>
                            <td>Développement spécifique</td>
                            <td><strong>Sur devis</strong></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clr"></div>
</div>

<!-- Tarifs -->
<?= $this->render('_prices_producer'); ?>
