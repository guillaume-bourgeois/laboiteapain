<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html ;
use yii\bootstrap\ActiveForm;

$this->setTitle('Producteur '.Html::encode($producer->name)) ;
$this->setMeta('description', 'Veuillez vous connecter pour réserver les produits de ce producteur.') ;

?>

<div id="page-producer">
    <h1 class="title-system-order"><?= Html::encode($producer->name) ?></h1>
    <p class="info"><span class="alert alert-warning">Veuillez vous connecter pour réserver les produits de ce producteur.</span></p>
    
    <?php if(isset($producer->photo) && strlen($producer->photo)): ?>
        <!--<img class="img-back" width="200px" height="auto" src="./uploads/<?= $producer->photo ?>" />-->
    <?php endif; ?>
    
    <div class="col-md-6">
        <h2>Connexion</h2>
        
        <?php if(isset($producer) && $producer->isDemo()) : ?>
            <div class="alert alert-warning">
                <p>Merci d'utiliser les identifiants suivants pour vous connecter à l'espace de démonstration :</p>
                Identifiant : <strong>demo@opendistrib.net</strong><br />
                Mot de passe : <strong>opendistrib</strong>
            </div>
        <?php endif; ?>
        <?php $form = ActiveForm::begin(['id' => 'login-form','enableClientValidation'=> false]); ?>
            <?= $form->field($loginForm, 'email') ?>
            <?= $form->field($loginForm, 'password')->passwordInput() ?>
            <?= $form->field($loginForm, 'rememberMe')->checkbox() ?>
            <p>
                Si vous avez oublié votre mot de passe, vous pouvez le <?= Html::a('réinitialiser', ['site/request-password-reset']) ?>.
            </p>
            <div class="form-group">
                <?= Html::submitButton('Connexion', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="col-md-6">
        <h2>Inscription</h2>
        <?php $form = ActiveForm::begin(['id' => 'form-signup','enableClientValidation'=> false]); ?>
            <?= $form->field($signupForm, 'email') ?>
            <?= $form->field($signupForm, 'password')->passwordInput() ?>
            <?= $form->field($signupForm, 'lastname') ?>
            <?= $form->field($signupForm, 'name') ?>
            <?= $form->field($signupForm, 'phone') ?>

            <?php if(strlen($producer->code)): ?>
                <?= $form->field($signupForm, 'code',[
                            'inputTemplate' => '<div class="input-group"><span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>{input}</div>',
                        ])
                        ->label('Code du producteur')
                        ->hint('Renseignez-vous auprès de votre producteur pour qu\'il vous fournisse le code d\'accès') ;  ?>
            <?php endif; ?>
    
            <div class="form-group" id="boutons-inscrire">
                <?= Html::submitButton("S'inscrire", ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
