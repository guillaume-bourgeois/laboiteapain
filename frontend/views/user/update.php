<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Mon profil' ;

?>
<div id="profil-user" class="user-update">

    <h1 class="title-system-order"><span class="glyphicon glyphicon-user"></span> Mon profil</h1>
    
    <div class="user-form">

        <?php $form = ActiveForm::begin([
             'enableClientScript' => false
        ]); ?>
        
        <h2>Informations</h2>
        <?= $form->field($model, 'lastname')->textInput() ?>
        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'phone')->textInput() ?>
        <?= $form->field($model, 'email')->textInput(['readonly' => true]); ?>
        <?= $form->field($model, 'address')->textarea() ?>
        
        <h2>Mot de passe</h2>
        <p>Renseignez les champs ci-dessous si vous souhaitez modifier votre mot de passe.</p>
        <?= $form->field($model, 'password_old')->passwordInput() ?>
        <?= $form->field($model, 'password_new')->passwordInput() ?>
        <?= $form->field($model, 'password_new_confirm')->passwordInput() ?>
        
        
        <div class="clr"></div>
        
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Modifier', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


</div>
