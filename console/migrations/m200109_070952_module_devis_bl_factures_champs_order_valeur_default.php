<?php

use yii\db\Migration;
use yii\db\Schema;

class m200109_070952_module_devis_bl_factures_champs_order_valeur_default extends Migration
{
    public function safeUp()
    {
            $this->alterColumn('order', 'id_point_sale', Schema::TYPE_INTEGER.' DEFAULT NULL') ;
            $this->alterColumn('order', 'id_distribution', Schema::TYPE_INTEGER.' DEFAULT NULL') ;
    }

    public function safeDown()
    {
            $this->alterColumn('order', 'id_point_sale', Schema::TYPE_INTEGER) ;
            $this->alterColumn('order', 'id_distribution', Schema::TYPE_INTEGER) ;
    }

}
