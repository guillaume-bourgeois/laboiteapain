<?php

use yii\db\Migration;

class m210303_090805_add_group_user_specific_prices extends Migration
{
        public function safeUp()
        {
                $this->addColumn('product_price', 'id_user_group', Schema::TYPE_INTEGER);
        }

        public function safeDown()
        {
                $this->dropColumn('product_price', 'id_user_group');
        }

}
