<?php

use yii\db\Migration;
use yii\db\Schema;

class m190118_084149_add_field_online_payment extends Migration {

    public function up() {
        $this->addColumn('producer', 'online_payment', Schema::TYPE_BOOLEAN) ;
    }

    public function down() {
        $this->dropColumn('producer', 'online_payment') ;
    }
    
}
