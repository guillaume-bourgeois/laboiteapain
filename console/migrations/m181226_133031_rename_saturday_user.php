<?php

use yii\db\Migration;

class m181226_133031_rename_saturday_user extends Migration {

    public function up() {
        $this->renameColumn('user', 'mail_distribution_saterday', 'mail_distribution_saturday');
    }

    public function down() {
        $this->renameColumn('user', 'mail_distribution_saturday', 'mail_distribution_saterday');
    }

}
