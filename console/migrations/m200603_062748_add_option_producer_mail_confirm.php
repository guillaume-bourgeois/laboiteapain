<?php

use yii\db\Migration;
use yii\db\Schema;

class m200603_062748_add_option_producer_mail_confirm extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_email_confirm', Schema::TYPE_BOOLEAN.' DEFAULT 0');
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_email_confirm');

                return false;
        }
}
