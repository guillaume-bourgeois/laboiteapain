<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190305_104710_producer_siret_non_requis extends Migration {

    public function up() {
        $this->alterColumn('producer', 'siret', Schema::TYPE_STRING.' DEFAULT NULL') ;
    }

    public function down() {
        $this->alterColumn('producer', 'siret', Schema::TYPE_STRING.' NOT NULL') ;
    }

}
