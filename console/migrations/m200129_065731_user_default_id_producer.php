<?php

use yii\db\Migration;
use yii\db\Schema;

class m200129_065731_user_default_id_producer extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user', 'id_producer', Schema::TYPE_INTEGER.' DEFAULT NULL') ;
    }

    public function safeDown()
    {
    }

}
