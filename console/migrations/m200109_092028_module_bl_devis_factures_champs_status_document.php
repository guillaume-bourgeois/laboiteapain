<?php

use yii\db\Migration;
use yii\db\Schema;
use common\models\Document ;

class m200109_092028_module_bl_devis_factures_champs_status_document extends Migration
{
    public function safeUp()
    {
            $this->addColumn('invoice', 'status', Schema::TYPE_STRING.' DEFAULT \''.Document::STATUS_DRAFT.'\'') ;
            $this->addColumn('delivery_note', 'status', Schema::TYPE_STRING.' DEFAULT \''.Document::STATUS_DRAFT.'\'') ;
            $this->addColumn('quotation', 'status', Schema::TYPE_STRING.' DEFAULT \''.Document::STATUS_DRAFT.'\'') ;
    }

    public function safeDown()
    {
            $this->dropColumn('invoice', 'status') ;
            $this->dropColumn('delivery_note', 'status') ;
            $this->dropColumn('quotation', 'status') ;
    }
}
