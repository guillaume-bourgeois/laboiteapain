<?php

use yii\db\Migration;
use yii\db\Schema;

class m200107_095559_module_bl_devis_factures_modification_champs_delivery_note extends Migration
{

        public function safeUp()
        {
                $this->dropColumn('delivery_note', 'id_point_sale_distribution');
                $this->addColumn('delivery_note', 'id_point_sale', Schema::TYPE_INTEGER);
                $this->addColumn('delivery_note', 'id_distribution', Schema::TYPE_INTEGER);
        }

        public function safeDown()
        {
                $this->addColumn('delivery_note', 'id_point_sale_distribution', Schema::TYPE_INTEGER);
                $this->dropColumn('delivery_note', 'id_point_sale');
                $this->dropColumn('delivery_note', 'id_distribution');
        }

}
