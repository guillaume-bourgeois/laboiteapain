<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

use yii\db\Migration;
use yii\db\Schema;

class m161222_100015_champs_point_vente_jours_livraison extends Migration
{
    public function up()
    {
        $this->addColumn('point_vente', 'livraison_lundi', Schema::TYPE_BOOLEAN) ;
        $this->addColumn('point_vente', 'livraison_mardi', Schema::TYPE_BOOLEAN) ;
        $this->addColumn('point_vente', 'livraison_mercredi', Schema::TYPE_BOOLEAN) ;
        $this->addColumn('point_vente', 'livraison_jeudi', Schema::TYPE_BOOLEAN) ;
        $this->addColumn('point_vente', 'livraison_vendredi', Schema::TYPE_BOOLEAN) ;
        $this->addColumn('point_vente', 'livraison_samedi', Schema::TYPE_BOOLEAN) ;
        $this->addColumn('point_vente', 'livraison_dimanche', Schema::TYPE_BOOLEAN) ;
        
        $this->createTable('production_point_vente', [
                'id_production' => Schema::TYPE_INTEGER.' NOT NULL',
                'id_point_vente' => Schema::TYPE_INTEGER.' NOT NULL',
                'livraison' => Schema::TYPE_BOOLEAN.' DEFAULT 0',
    	]);
        
        $this->addPrimaryKey('production_point_vente_pk', 'production_point_vente', ['id_production', 'id_point_vente']);
    }

    public function down()
    {
        $this->dropColumn('point_vente', 'livraison_lundi') ;
        $this->dropColumn('point_vente', 'livraison_mardi') ;
        $this->dropColumn('point_vente', 'livraison_mercredi') ;
        $this->dropColumn('point_vente', 'livraison_jeudi') ;
        $this->dropColumn('point_vente', 'livraison_vendredi') ;
        $this->dropColumn('point_vente', 'livraison_samedi') ;
        $this->dropColumn('point_vente', 'livraison_dimanche') ;
        
        $this->dropTable('production_point_vente') ;
    }
}
