<?php

use yii\db\Migration;
use yii\db\Schema;

class m200117_144356_user_ajout_champs_type extends Migration
{
        public function safeUp()
        {
                $this->addColumn('user', 'type', Schema::TYPE_STRING.' DEFAULT \''.User::TYPE_INDIVIDUAL.'\'');
                $this->addColumn('user', 'name_legal_person', Schema::TYPE_STRING) ;
        }

        public function safeDown()
        {
                $this->dropColumn('user', 'type') ;
                $this->dropColumn('user', 'name_legal_person') ;
        }
}

