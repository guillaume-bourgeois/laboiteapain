<?php

use yii\db\Migration;
use yii\db\Schema;

class m200108_101042_module_bl_devis_factures_modification_champs_documents extends Migration
{
        public function safeUp()
        {
                $this->addColumn('invoice', 'id_producer', Schema::TYPE_INTEGER);
                $this->addColumn('quotation', 'id_producer', Schema::TYPE_INTEGER);
                $this->addColumn('delivery_note', 'id_producer', Schema::TYPE_INTEGER);

                $this->alterColumn('invoice', 'date', Schema::TYPE_DATETIME . ' DEFAULT CURRENT_TIMESTAMP');
                $this->alterColumn('delivery_note', 'date', Schema::TYPE_DATETIME . ' DEFAULT CURRENT_TIMESTAMP');
                $this->alterColumn('quotation', 'date', Schema::TYPE_DATETIME . ' DEFAULT CURRENT_TIMESTAMP');
        }

        public function safeDown()
        {
                $this->dropColumn('invoice', 'id_producer');
                $this->dropColumn('delivery_note', 'id_producer');
                $this->dropColumn('quotation', 'id_producer');

                $this->alterColumn('invoice', 'date', Schema::TYPE_DATETIME);
                $this->alterColumn('delivery_note', 'date', Schema::TYPE_DATETIME);
                $this->alterColumn('quotation', 'date', Schema::TYPE_DATETIME);
        }

}
