<?php

use yii\db\Migration;
use yii\db\Schema;

class m200225_153956_document_display_orders extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'document_display_orders_invoice', Schema::TYPE_BOOLEAN ) ;
                $this->addColumn('producer', 'document_display_orders_delivery_note', Schema::TYPE_BOOLEAN ) ;
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'document_display_orders_invoice') ;
                $this->dropColumn('producer', 'document_display_orders_delivery_note') ;
        }

}
