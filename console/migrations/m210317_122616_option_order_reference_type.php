<?php

use yii\db\Migration;

class m210317_122616_option_order_reference_type extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_order_reference_type', Schema::TYPE_STRING);
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_order_reference_type');
        }
}
