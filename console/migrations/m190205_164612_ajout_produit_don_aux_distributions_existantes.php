<?php

use yii\db\Migration;

class m190205_164612_ajout_produit_don_aux_distributions_existantes extends Migration {

    public function up() {
        $distributionsArray = common\models\Distribution::find()->all() ;
        foreach($distributionsArray as $distribution) {
            $distribution->linkProductGift() ;
        }
    }

    public function down() {
        
    }
}
