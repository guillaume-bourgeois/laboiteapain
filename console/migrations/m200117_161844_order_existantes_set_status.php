<?php

use yii\db\Migration;

class m200117_161844_order_existantes_set_status extends Migration
{
    public function safeUp()
    {
        Order::updateAll([
                'status' => 'tmp-order'
        ]) ;
    }

    public function safeDown()
    {

    }

}
