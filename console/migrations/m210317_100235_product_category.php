<?php

use yii\db\Migration;
use yii\db\Schema;

class m210317_100235_product_category extends Migration
{
        public function safeUp()
        {
                $this->createTable('product_category', [
                        'id' => 'pk',
                        'id_producer' => Schema::TYPE_INTEGER . ' NOT NULL',
                        'name' => Schema::TYPE_STRING . ' NOT NULL',
                        'position' => Schema::TYPE_INTEGER . ' DEFAULT 0 NOT NULL',
                ]);

                $this->addColumn('product', 'id_product_category', Schema::TYPE_INTEGER) ;
        }

        public function safeDown()
        {
                $this->dropTable('product_category') ;
                $this->dropColumn('product', 'id_product_category') ;
        }

}
