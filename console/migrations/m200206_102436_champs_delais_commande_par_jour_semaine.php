<?php

use yii\db\Migration;
use yii\db\Schema;

class m200206_102436_champs_delais_commande_par_jour_semaine extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'order_delay_monday', Schema::TYPE_INTEGER) ;
                $this->addColumn('producer', 'order_deadline_monday', Schema::TYPE_FLOAT) ;
                $this->addColumn('producer', 'order_delay_tuesday', Schema::TYPE_INTEGER) ;
                $this->addColumn('producer', 'order_deadline_tuesday', Schema::TYPE_FLOAT) ;
                $this->addColumn('producer', 'order_delay_wednesday', Schema::TYPE_INTEGER) ;
                $this->addColumn('producer', 'order_deadline_wednesday', Schema::TYPE_FLOAT) ;
                $this->addColumn('producer', 'order_delay_thursday', Schema::TYPE_INTEGER) ;
                $this->addColumn('producer', 'order_deadline_thursday', Schema::TYPE_FLOAT) ;
                $this->addColumn('producer', 'order_delay_friday', Schema::TYPE_INTEGER) ;
                $this->addColumn('producer', 'order_deadline_friday', Schema::TYPE_FLOAT) ;
                $this->addColumn('producer', 'order_delay_saturday', Schema::TYPE_INTEGER) ;
                $this->addColumn('producer', 'order_deadline_saturday', Schema::TYPE_FLOAT) ;
                $this->addColumn('producer', 'order_delay_sunday', Schema::TYPE_INTEGER) ;
                $this->addColumn('producer', 'order_deadline_sunday', Schema::TYPE_FLOAT) ;
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'order_delay_monday') ;
                $this->dropColumn('producer', 'order_deadline_monday') ;
                $this->dropColumn('producer', 'order_delay_tuesday') ;
                $this->dropColumn('producer', 'order_deadline_tuesday') ;
                $this->dropColumn('producer', 'order_delay_wednesday') ;
                $this->dropColumn('producer', 'order_deadline_wednesday') ;
                $this->dropColumn('producer', 'order_delay_thursday') ;
                $this->dropColumn('producer', 'order_deadline_thursday') ;
                $this->dropColumn('producer', 'order_delay_friday') ;
                $this->dropColumn('producer', 'order_deadline_friday') ;
                $this->dropColumn('producer', 'order_delay_saturday') ;
                $this->dropColumn('producer', 'order_deadline_saturday') ;
                $this->dropColumn('producer', 'order_delay_sunday') ;
                $this->dropColumn('producer', 'order_deadline_sunday') ;
        }
}
