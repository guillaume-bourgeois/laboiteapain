<?php

use yii\db\Migration;

class m181228_074109_rename_db_content extends Migration {

    public function up() {
        $this->update('credit_history', ['type' => 'payment'], "type = 'paiement'");
        $this->update('credit_history', ['type' => 'refund'], "type = 'remboursement'");

        $this->update('credit_history', ['mean_payment' => 'money'], "mean_payment = 'especes'");
        $this->update('credit_history', ['mean_payment' => 'credit-card'], "mean_payment = 'cb'");
        $this->update('credit_history', ['mean_payment' => 'other'], "mean_payment = 'autre'");
        $this->update('credit_history', ['mean_payment' => 'initial-credit'], "mean_payment = 'credit-initial'");

        $this->update('development_priority', ['priority' => 'high'], "priority = 'haute'");
        $this->update('development_priority', ['priority' => 'normal'], "priority = 'normale'");
        $this->update('development_priority', ['priority' => 'low'], "priority = 'basse'");

        $this->update('product', ['sale_mode' => 'unit'], "sale_mode = 'unite'");
        $this->update('product', ['sale_mode' => 'weight'], "sale_mode = 'poids'");

        $this->update('product_order', ['sale_mode' => 'unit'], "sale_mode = 'unite'");
        $this->update('product_order', ['sale_mode' => 'weight'], "sale_mode = 'poids'");
    }

    public function down() {
        $this->update('credit_history', ['type' => 'paiement'], "type = 'payment'") ;
        $this->update('credit_history', ['type' => 'remboursement'], "type = 'refund'") ;
        
        $this->update('credit_history', ['mean_payment' => 'especes'], "mean_payment = 'money'") ;
        $this->update('credit_history', ['mean_payment' => 'cb'], "mean_payment = 'credit-card'") ;
        $this->update('credit_history', ['mean_payment' => 'autre'], "mean_payment = 'other'") ;
        $this->update('credit_history', ['mean_payment' => 'credit-initial'], "mean_payment = 'initial-credit'") ;
        
        $this->update('development_priority', ['priority' => 'haute'], "priority = 'high'") ;
        $this->update('development_priority', ['priority' => 'normale'], "priority = 'normal'") ;
        $this->update('development_priority', ['priority' => 'basse'], "priority = 'low'") ;
        
        $this->update('product', ['sale_mode' => 'unite'], "sale_mode = 'unit'") ;
        $this->update('product', ['sale_mode' => 'poids'], "sale_mode = 'weight'") ;
        
        $this->update('product_order', ['sale_mode' => 'unite'], "sale_mode = 'unit'") ;
        $this->update('product_order', ['sale_mode' => 'poids'], "sale_mode = 'weight'") ;
    }
}
