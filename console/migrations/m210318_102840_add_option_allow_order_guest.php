<?php

use yii\db\Migration;

class m210318_102840_add_option_allow_order_guest extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_allow_order_guest', Schema::TYPE_BOOLEAN);
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_allow_order_guest');
        }
}
