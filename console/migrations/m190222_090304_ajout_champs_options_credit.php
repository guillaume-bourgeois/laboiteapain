<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m190222_090304_ajout_champs_options_credit extends Migration {

    public function up() {
        $this->addColumn('producer', 'credit_limit', Schema::TYPE_FLOAT) ;
        $this->addColumn('producer', 'use_credit_checked_default', Schema::TYPE_BOOLEAN.' DEFAULT 1') ;
    }

    public function down() {
        $this->dropColumn('producer', 'credit_limit') ;
        $this->dropColumn('producer', 'use_credit_checked_default') ;
    }
}
