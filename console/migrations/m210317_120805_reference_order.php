<?php

use yii\db\Migration;
use yii\db\Schema;

class m210317_120805_reference_order extends Migration
{
        public function safeUp()
        {
                $this->addColumn('order', 'reference', Schema::TYPE_STRING) ;
        }

        public function safeDown()
        {
                $this->dropColumn('order', 'reference') ;
        }
}
