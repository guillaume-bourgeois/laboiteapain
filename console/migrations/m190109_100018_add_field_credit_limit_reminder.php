<?php

use yii\db\Migration ;
use yii\db\mysql\Schema ;

class m190109_100018_add_field_credit_limit_reminder extends Migration {

    public function up() {
        $this->addColumn('producer', 'credit_limit_reminder', Schema::TYPE_FLOAT) ;
    }

    public function down() {
        $this->dropColumn('producer', 'credit_limit_reminder') ;
    }
}
