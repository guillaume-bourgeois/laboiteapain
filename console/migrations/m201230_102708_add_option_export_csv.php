<?php

use yii\db\Migration;

class m201230_102708_add_option_export_csv extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_csv_export_by_piece', Schema::TYPE_BOOLEAN.' DEFAULT 0');
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_csv_export_by_piece');

                return false;
        }
}
