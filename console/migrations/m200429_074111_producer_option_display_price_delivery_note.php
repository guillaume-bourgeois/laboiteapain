<?php

use yii\db\Migration;
use yii\db\Schema;

class m200429_074111_producer_option_display_price_delivery_note extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'document_display_prices_delivery_note', Schema::TYPE_BOOLEAN) ;
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'document_display_prices_delivery_note') ;
        }
}
