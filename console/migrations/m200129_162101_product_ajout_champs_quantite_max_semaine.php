<?php

use yii\db\Migration;
use yii\db\Schema;

class m200129_162101_product_ajout_champs_quantite_max_semaine extends Migration
{
        public function safeUp()
        {
                $this->alterColumn('product', 'quantity_max', Schema::TYPE_FLOAT.' DEFAULT NULL') ;
                $this->addColumn('product', 'quantity_max_monday', Schema::TYPE_FLOAT . ' DEFAULT NULL');
                $this->addColumn('product', 'quantity_max_tuesday', Schema::TYPE_FLOAT . ' DEFAULT NULL');
                $this->addColumn('product', 'quantity_max_wednesday', Schema::TYPE_FLOAT.' DEFAULT NULL') ;
                $this->addColumn('product', 'quantity_max_thursday', Schema::TYPE_FLOAT.' DEFAULT NULL') ;
                $this->addColumn('product', 'quantity_max_friday', Schema::TYPE_FLOAT.' DEFAULT NULL') ;
                $this->addColumn('product', 'quantity_max_saturday', Schema::TYPE_FLOAT.' DEFAULT NULL') ;
                $this->addColumn('product', 'quantity_max_sunday', Schema::TYPE_FLOAT.' DEFAULT NULL') ;
        }

        public function safeDown()
        {
                $this->alterColumn('product', 'quantity_max', Schema::TYPE_INTEGER) ;
                $this->dropColumn('product', 'quantity_max_monday');
                $this->dropColumn('product', 'quantity_max_tuesday');
                $this->dropColumn('product', 'quantity_max_wednesday') ;
                $this->dropColumn('product', 'quantity_max_thursday') ;
                $this->dropColumn('product', 'quantity_max_friday') ;
                $this->dropColumn('product', 'quantity_max_saturday') ;
                $this->dropColumn('product', 'quantity_max_sunday') ;
        }
}
