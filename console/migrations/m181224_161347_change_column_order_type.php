<?php

use yii\db\Migration;

class m181224_161347_change_column_order_type extends Migration {

    public function up() {
        $this->renameColumn('order', 'type', 'origin');
    }

    public function down() {
        $this->renameColumn('order', 'origin', 'type');
    }

}
