<?php

use yii\db\Migration;
use yii\db\Schema;

class m210326_104759_add_option_order_entry_point extends Migration
{
        public function safeUp()
        {
                $this->addColumn('producer', 'option_order_entry_point', Schema::TYPE_STRING.' DEFAULT \''.Producer::ORDER_ENTRY_POINT_DATE.'\'');
        }

        public function safeDown()
        {
                $this->dropColumn('producer', 'option_order_entry_point');
        }

}
