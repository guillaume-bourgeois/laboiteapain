<?php

/**
 * Copyright distrib (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

namespace producer\controllers;

use common\helpers\GlobalParam;
use common\models\Producer;
use common\models\ProductCategory;

class SiteController extends ProducerBaseController
{

        /**
         * @inheritdoc
         */
        public function behaviors()
        {
                return [];
        }

        public function actions()
        {
                return [
                        'captcha' => [
                                'class' => 'yii\captcha\CaptchaAction',
                                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                        ],
                ];
        }

        /**
         * Affiche et gère les erreurs.
         *
         * @return mixed
         */
        public function actionError()
        {
                $exception = Yii::$app->errorHandler->exception;

                if ($exception !== null) {
                        if ($exception->getMessage() == 'Producteur introuvable') {
                                Yii::$app->getResponse()->redirect(Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/error', 'producer_not_found' => true]))->send();
                                return;
                        } else {
                                return $this->render('error', ['exception' => $exception]);
                        }
                }
        }

        /**
         * Affiche la page d'accueil des producteurs comprenant une image, une
         * description, la liste des points de vente et les produits
         *
         * @return ProducerView
         */
        public function actionIndex()
        {
                // points de vente
                $dataProviderPointsSale = new ActiveDataProvider([
                        'query' => PointSale::find()
                                ->where([
                                        'id_producer' => $this->getProducer()->id,
                                        'restricted_access' => 0
                                ]),
                        'pagination' => [
                                'pageSize' => 50,
                        ],
                        'sort' => false,
                ]);

                // produits
                $categoriesArray = ProductCategory::searchAll([], ['orderby' => 'product_category.position ASC']) ;
                $dataProviderProductsByCategories = [] ;
                foreach($categoriesArray as $category) {
                        $dataProviderProductsByCategories[$category->id] = new ActiveDataProvider([
                                'query' => Product::find()
                                        ->andWhere([
                                                'id_producer' => $this->getProducer()->id,
                                                'active' => true,
                                        ])
                                        ->andWhere('product.id_product_category = :id_product_category')
                                        ->params([':id_product_category' => $category->id])
                                        ->orderBy('order ASC'),
                                'pagination' => [
                                        'pageSize' => 500,
                                ],
                                'sort' => false,
                        ]);
                }

                $queryProducts = Product::find()
                        ->andWhere([
                                'id_producer' => $this->getProducer()->id,
                                'active' => true,
                                'id_product_category' => null,
                        ])
                        ->orderBy('order ASC') ;

                $dataProviderProducts = new ActiveDataProvider([
                        'query' => $queryProducts,
                        'pagination' => [
                                'pageSize' => 500,
                        ],
                        'sort' => false,
                ]);

                $products = $queryProducts->all() ;
                foreach($dataProviderProductsByCategories as $dataProvider) {
                        $products = array_merge(
                                $products,
                                $dataProvider->query->all()
                        ) ;
                }

                $hasProductPhoto = false ;
                $hasProductWeight = false ;

                foreach($products as $product) {
                        if(strlen($product->photo) > 0) {
                                $hasProductPhoto = true ;
                        }
                        if($product->weight && $product->weight > 0) {
                                $hasProductWeight = true ;
                        }
                }

                return $this->render('index', [
                        'dataProviderProductsByCategories' => $dataProviderProductsByCategories,
                        'dataProviderPointsSale' => $dataProviderPointsSale,
                        'dataProviderProducts' => $dataProviderProducts,
                        'hasProductPhoto' => $hasProductPhoto,
                        'hasProductWeight' => $hasProductWeight,
                        'categories' => $categoriesArray,
                ]);
        }

        /**
         * Affiche et traite le formulaire de contact dédié aux producteurs
         *
         * @return ProducerView
         */
        public function actionContact()
        {
                $model = new ContactForm();
                $producer = $this->getProducer();

                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                        $isSent = false ;
                        if (is_array($producer->contact)) {
                                $email = '' ;
                                $contact = $producer->getMainContact() ;
                                if($contact) {
                                        $email = $contact->email ;
                                }

                                if(strlen($email) && $model->sendEmail($email)) {
                                        $isSent = true ;
                                }
                        }

                        if($isSent) {
                                Yii::$app->session->setFlash('success', 'Votre message a bien été envoyé.');
                        }
                        else {
                                Yii::$app->session->setFlash('error', 'Il y a eu une erreur lors de l\'envoi de votre message.');
                        }

                        return $this->refresh();
                } else {
                        return $this->render('contact', [
                                'model' => $model,
                        ]);
                }
        }

        /**
         * Ajoute ou supprime un producteur des favoris de l'utilisateur.
         * Redirige vers la page d'accueil du producteur.
         *
         * @param $action 'add' ou 'delete'
         */
        public function actionBookmarks($action)
        {
                $producer = $this->getProducer();
                $userProducer = UserProducer::find()
                        ->where([
                                'id_user' => User::getCurrentId(),
                                'id_producer' => $producer->id
                        ])
                        ->one();

                if (!$userProducer) {
                        $userProducer = Producer::addUser(User::getCurrentId(), $producer->id);
                }

                if ($userProducer) {
                        if ($action == 'add') {
                                $userProducer->bookmark = 1;
                                Yii::$app->session->setFlash('success', 'Le producteur <strong>' . Html::encode($producer->name) . '</strong> vient d\'être ajouté à vos favoris.');
                        } else {
                                $userProducer->bookmark = 0;
                                Yii::$app->session->setFlash('success', 'Le producteur <strong>' . Html::encode($producer->name) . '</strong> vient d\'être supprimé de vos favoris.');
                        }
                        $userProducer->save();
                }

                $this->redirect(['site/index']);
        }

        /**
         * Affiche les mentions légales du producteur.
         *
         * @return ProducerView
         */
        public function actionMentions()
        {
                $producer = GlobalParam::getCurrentProducer();

                if (!strlen($producer->mentions)) {
                        throw new \yii\base\UserException('Mentions légales introuvables.');
                }

                return $this->render('mentions', [
                        'producer' => $producer
                ]);
        }

        /**
         * Affiche les conditions générales de vente du producteur.
         *
         * @return ProducerView
         */
        public function actionGcs()
        {
                $producer = GlobalParam::getCurrentProducer();

                if (!strlen($producer->gcs)) {
                        throw new \yii\base\UserException('Conditions générales de vente introuvables.');
                }

                return $this->render('gcs', [
                        'producer' => $producer
                ]);
        }
}

?>