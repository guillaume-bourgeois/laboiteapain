<?php

/** 
Copyright distrib (2018) 

contact@opendistrib.net

Ce logiciel est un programme informatique servant à aider les producteurs 
à distribuer leur production en circuits courts. 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.
*/

namespace producer\controllers;

class ProducerBaseController extends CommonController 
{

    var $producer ;
    
    /**
     * @inheritdoc
     */
    public function behaviors() 
    {
        return [];
    }
   
    public function actions() 
    {
        return [];
    }
    
    public function beforeAction($event) 
    {
        $producer = $this->getProducer() ;
        
        $userProducer = UserProducer::find()
            ->where([
                'id_user' => User::getCurrentId(),
                'id_producer' => $producer->id
            ])
            ->one() ;
        
        /*
         *  Producteur protègé par un code
         */
        if(strlen($producer->code)) {
            
            // Si l'utilisateur n'est pas connecté, on le redirige vers une page qui lui permet
            // de se connecter ou de s'inscrire en saisissant le code du producteur
            if(Yii::$app->user->isGuest) {
                $this->redirect(Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/producer','id' => $producer->id])) ;
            }
            // si l'utilisateur est connecté et qu'il n'a pas encore saisi de code
            else {
                if(!$userProducer || ($userProducer && !$userProducer->active)) {
                    $this->redirect(Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/producer-code','id' => $producer->id])) ;
                }
            }
        }
        
        /*
         * Producteur hors ligne
         */
        if(!$producer->active && (Yii::$app->user->isGuest || Yii::$app->user->identity->id_producer != $producer->id)) {
            $this->redirect(Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/producer-offline','id' => $producer->id])) ;
        }
        
        return parent::beforeAction($event);
    }
    
    /**
     * Retourne le producteur courant.
     * 
     * @return Etablissement
     * @throws \yii\web\HttpException
     */
    public function getProducer() 
    {
        if($this->producer) {
            return $this->producer ;
        }
        else {
            $producer = Producer::find()
                ->with('contact')
                ->where(['slug' => Yii::$app->getRequest()->getQueryParam('slug_producer')])
                ->one() ;
            
            if($producer) {
                $this->producer = $producer ;
                return $this->producer ;
            }   
            else {
                throw new \yii\web\HttpException(404, 'Producteur introuvable');
            }
        }
    }
}

?>