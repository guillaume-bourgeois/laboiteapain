<?php

/**
 * Copyright La boîte à pain (2018)
 *
 * contact@opendistrib.net
 *
 * Ce logiciel est un programme informatique servant à aider les producteurs
 * à distribuer leur production en circuits courts.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".
 *
 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.
 *
 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.
 *
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\helpers\Url;
use common\models\Producer;
use common\models\User;
use common\helpers\GlobalParam;

\common\assets\CommonAsset::register($this);
\producer\assets\AppAsset::register($this);

$producer = $this->context->getProducer();
if (!Yii::$app->user->isGuest) {
        $userProducer = UserProducer::findOne(['id_user' => User::getCurrentId(), 'id_producer' => $producer->id]);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<head>
    <title><?= Html::encode($producer->name); ?> | <?= $this->getPageTitle(); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="base-url" content="<?= Yii::$app->urlManager->baseUrl; ?>">
    <meta name="slug-producer" content="<?= $producer->slug; ?>">
        <?= Html::csrfMetaTags() ?>
    <link rel="icon" type="image/png"
          href="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/favicon-distrib.png"/>
        <?php $this->head() ?>
</head>
<body class="<?= Yii::$app->controller->id . '-' . Yii::$app->controller->action->id ?>">
<?php $this->beginBody() ?>

<div id="header-bap">
        <?php
        echo Nav::widget([
                'encodeLabels' => false,
                'items' => [
                        [
                                'label' => '<span class="glyphicon glyphicon-menu-hamburger"></span>',
                                'options' => ['id' => 'label1'],
                                'url' => '#',
                                'items' => [
                                        [
                                                'label' => '<span class="glyphicon glyphicon-chevron-left"></span> Retour à l\'accueil',
                                                'url' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/index']),
                                                'visible' => Yii::$app->user->isGuest
                                        ],
                                        [
                                                'label' => '<span class="glyphicon glyphicon-user"></span> Inscription',
                                                'url' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/signup']),
                                                'visible' => Yii::$app->user->isGuest
                                        ],
                                        [
                                                'label' => '<span class="glyphicon glyphicon-log-in"></span> Connexion',
                                                'url' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/login', 'return_url' => Yii::$app->urlManager->createAbsoluteUrl(['site/index'])]),
                                                'visible' => Yii::$app->user->isGuest
                                        ],
                                ],
                                'visible' => Yii::$app->user->isGuest
                        ],

                        [
                                'label' => '<span class="glyphicon glyphicon-menu-hamburger"></span>',
                                'options' => ['id' => 'label1'],
                                'url' => '#',
                                'items' => [
                                        [
                                                'label' => '<span class="glyphicon glyphicon-chevron-left"></span> Retour à l\'accueil',
                                                'url' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/index']),
                                        ],
                                        [
                                                'label' => '<span class="glyphicon glyphicon-user"></span> Profil',
                                                'url' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['user/update']),
                                        ],
                                        [
                                                'label' => '<span class="glyphicon glyphicon-off"></span> Déconnexion<br /><small>' . (!Yii::$app->user->isGuest ? Html::encode(Yii::$app->user->identity->name . ' ' . strtoupper(substr(Yii::$app->user->identity->lastname, 0, 1))) : '') . '.</small>',
                                                'url' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/logout']),
                                        ]
                                ],
                                'visible' => !Yii::$app->user->isGuest
                        ],
                ],
                'options' => ['id' => 'nav-bap'],
        ]);
        ?>
</div>

<div class="container">
    <div id="left" class="col-md-3">
        <div class="fixed">
                <?php if (strlen($producer->logo)): ?>
                    <div id="logo"<?php if (!is_null($producer->background_color_logo) && strlen($producer->background_color_logo)): ?> style="background-color:<?= Html::encode($producer->background_color_logo); ?>"<?php endif; ?>>
                        <a href="<?= Yii::$app->urlManager->createUrl(['site/index']) ?>">
                            <img class="img-logo"
                                 src="<?= Yii::$app->urlManager->baseUrl; ?>/uploads/<?= $producer->logo; ?>"
                                 alt="Logo <?= Html::encode($producer->name) ?>"/>
                        </a>
                    </div>
                <?php endif; ?>
            <h1><?= Html::encode($producer->name); ?></h1>
            <h2><?= Html::encode($producer->type) ?> à <?= Html::encode($producer->city); ?>
                (<?= Html::encode($producer->postcode); ?>)</h2>

            <nav id="main-nav">
                    <?php

                    $credit = '';
                    if (isset($userProducer) && $userProducer) {
                            $labelType = $userProducer->credit > 0 ? 'success' : 'danger';
                            $credit = ' <span class="label label-' . $labelType . '">' . number_format($userProducer->credit, 2) . ' €</span>';
                    }

                    $countSubcriptions = Subscription::find()
                            ->where([
                                    'subscription.id_user' => User::getCurrentId(),
                                    'subscription.id_producer' => GlobalParam::getCurrentProducerId(),
                            ])->count();
                    $labelSubscription = $countSubcriptions > 0 ? 'success' : 'default';

                    $countOrders = Order::find()
                            ->joinWith(['distribution'])
                            ->where([
                                    'id_user' => User::getCurrentId(),
                                    'distribution.id_producer' => GlobalParam::getCurrentProducerId()
                            ])
                            ->params([':date_today' => date('Y-m-d')])
                            ->andWhere('distribution.date >= :date_today')
                            ->count();
                    $labelOrders = $countOrders > 0 ? 'success' : 'default';


                    echo Nav::widget([
                            'encodeLabels' => false,
                            'options' => ['class' => 'nav'],
                            'items' => [
                                    [
                                            'label' => '<span class="glyphicon glyphicon-th-large"></span> Accueil',
                                            'url' => Yii::$app->urlManager->createUrl(['site/index']),
                                            'active' => $this->getControllerAction() == 'site/index',
                                    ],
                                    [
                                            'label' => '<span class="glyphicon glyphicon-plus"></span> Commander',
                                            'url' => Yii::$app->urlManager->createUrl(['order/order']),
                                            'visible' => $producer->option_allow_order_guest || !Yii::$app->user->isGuest,
                                            'active' => $this->getControllerAction() == 'order/order',
                                    ],
                                    [
                                            'label' => '<span class="glyphicon glyphicon-plus"></span> Commander',
                                            'url' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/producer', 'id' => $this->context->getProducer()->id, 'return_url' => Yii::$app->urlManagerProducer->createAbsoluteUrl(['order/order', 'slug_producer' => $this->context->getProducer()->slug])]),
                                            'visible' => !$producer->option_allow_order_guest && Yii::$app->user->isGuest,
                                            'active' => $this->getControllerAction() == 'order/order',
                                    ],
                                    [
                                            'label' => '<span class="glyphicon glyphicon-folder-open"></span> Mes commandes <span class="label label-' . $labelOrders . '">' . $countOrders . '</span>',
                                            'url' => Yii::$app->urlManager->createUrl(['order/history']),
                                            'visible' => !Yii::$app->user->isGuest,
                                            'active' => $this->getControllerAction() == 'order/history',
                                    ],
                                    [
                                            'label' => '<span class="glyphicon glyphicon-repeat"></span> Abonnements <span class="label label-' . $labelSubscription . '">' . $countSubcriptions . '</span>',
                                            'url' => Yii::$app->urlManager->createUrl(['subscription/index']),
                                            'visible' => !Yii::$app->user->isGuest && $producer->user_manage_subscription,
                                            'active' => $this->getControllerAction() == 'subscription/index',
                                    ],
                                    [
                                            'label' => '<span class="glyphicon glyphicon-euro"></span> Crédit' . $credit,
                                            'url' => Yii::$app->urlManager->createUrl(['credit/history']),
                                            'visible' => !Yii::$app->user->isGuest && $producer->credit,
                                            'active' => $this->getControllerAction() == 'credit/history',
                                    ],
                                    [
                                            'label' => '<span class="glyphicon glyphicon-envelope"></span> Contact',
                                            'url' => Yii::$app->urlManager->createUrl(['site/contact']),
                                            'active' => $this->getControllerAction() == 'site/contact',
                                    ],
                                    [
                                            'label' => '<span class="glyphicon glyphicon-cog"></span> Administration',
                                            'url' => Yii::$app->urlManagerBackend->createAbsoluteUrl(['site/index']),
                                            'visible' => isset(Yii::$app->user->identity) && Yii::$app->user->identity->isProducer(),
                                            'options' => ['id' => 'btn-administration']
                                    ],
                            ],
                    ]);
                    ?>
            </nav>
        </div>
    </div>

    <div id="main" class="col-md-9">
            <?php if (strlen($producer->photo)): ?>
                <div id="img-big">
                    <img class="img-photo" src="<?= Yii::$app->urlManager->baseUrl; ?>/uploads/<?= $producer->photo; ?>"
                         alt="Photo <?= Html::encode($producer->name) ?>"/>
                </div>
            <?php endif; ?>
        <div id="infos-producer">
                <?php if(!$producer->hasSpecificDelays() && $producer->order_deadline && $producer->order_delay): ?>
                        <span data-toggle="tooltip" data-placement="bottom" title="Heure limite de commande">
                            <span class="glyphicon glyphicon-time"></span> Commande avant
                            <strong><?php echo Html::encode($producer->order_deadline) ?> h</strong></span>,
                            <span data-toggle="tooltip" data-placement="bottom"
                                  title="Exemple : commande le lundi pour le
                                  <?php if ($producer->order_delay == 1): ?>mardi<?php elseif ($producer->order_delay == 2): ?>mercredi
                                  <?php elseif ($producer->order_delay == 3): ?>jeudi<?php elseif ($producer->order_delay == 4): ?>vendredi
                                  <?php elseif ($producer->order_delay == 5): ?>samedi<?php elseif ($producer->order_delay == 6): ?>dimanche
                                  <?php elseif ($producer->order_delay == 7): ?>lundi d'après<?php endif; ?>"><strong><?= Html::encode($producer->order_delay) ?> jour<?php if ($producer->order_delay > 1): ?>s<?php endif; ?></strong> à l'avance</span>
                <?php endif; ?>

                <?php if (!Yii::$app->user->isGuest): ?>
                    <span class="favorite">
                        <?php if ($userProducer && $userProducer->bookmark): ?>
                            <span class="glyphicon glyphicon-star"></span> <a
                                    href="<?= Yii::$app->urlManagerProducer->createUrl(['site/bookmarks', 'action' => 'delete']); ?>"
                                    data-toggle="tooltip" data-placement="bottom" title="Supprimer de mes favoris">Favoris</a>
                        <?php else: ?>
                            <span class="glyphicon glyphicon glyphicon-star-empty"></span> <a
                                    href="<?= Yii::$app->urlManagerProducer->createUrl(['site/bookmarks', 'action' => 'add']); ?>"
                                    data-toggle="tooltip" data-placement="bottom"
                                    title="Ajouter à mes favoris">Favoris</a>
                        <?php endif; ?>
                    </span>
                <?php endif; ?>

            <div class="clr"></div>
        </div>

            <?php if (strlen($this->getTitle())): ?>
                <h2 id="page-title">
                        <?= $this->getTitle(); ?>
                        <?php
                        if (count($this->buttons)): ?>
                            <span id="buttons">
                            <?php foreach ($this->buttons as $button) {
                                    echo '<a href="' . Yii::$app->urlManagerProducer->createUrl($button['url']) . '" class="' . $button['class'] . '">' . $button['label'] . '</a>';
                            }
                            ?>
                            </span>
                        <?php endif; ?>
                </h2>
            <?php endif; ?>

        <section id="content">
                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <div class="alert alert-danger" role="alert">
                            <?= Yii::$app->session->getFlash('error') ?>
                    </div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success" role="alert">
                            <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                <?php endif; ?>

                <?= $content ?>
        </section>

            <?php if (strlen($producer->mentions) || strlen($producer->gcs)): ?>
                <section id="footer-producer">
                        <?= Html::encode($producer->name) ?> :
                        <?php if (strlen($producer->mentions)): ?>
                            <a href="<?php echo Yii::$app->urlManager->createUrl(['site/mentions']); ?>">Mentions
                                légales</a>
                        <?php endif; ?>
                        <?php if (strlen($producer->mentions) && strlen($producer->gcs)): ?>
                            &bull;
                        <?php endif; ?>
                        <?php if (strlen($producer->gcs)): ?>
                            <a href="<?php echo Yii::$app->urlManager->createUrl(['site/gcs']); ?>">Conditions générales
                                de vente</a>
                        <?php endif; ?>
                </section>
            <?php endif; ?>
    </div>
</div>

<footer id="footer" class="col-md-9">
    <div class="content">
        <a href="<?php echo Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/index']); ?>">distrib</a> &bull;
        <a href="<?php echo Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/mentions']); ?>">Mentions
            légales</a> &bull;
        <a href="<?php echo Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/cgv']); ?>">CGS</a> &bull;
        <a id="code-source" href="https://framagit.org/guillaume-bourgeois/distrib">Code source <img
                    src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-framagit.png"
                    alt="Hébergé par Framasoft"/> <img
                    src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-gitlab.png"
                    alt="Propulsé par Gitlab"/></a> &bull;
        <a id="social-mastodon" href="https://mastodon.social/@opendistrib">Mastodon <img
                    src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-mastodon.png" alt="Mastodon"/></a>
        &bull;
        <a id="social-diaspora" href="https://framasphere.org/people/db12d640c64c0137f1d52a0000053625">Diaspora <img
                    src="<?php echo Yii::$app->urlManager->getBaseUrl(); ?>/img/logo-diaspora.png" alt="Diaspora"/></a>
    </div>
</footer>

<?php $this->endBody() ?>
<?= $this->render('@common/views/_analytics.php'); ?>
</body>
</html>
<?php $this->endPage() ?>
